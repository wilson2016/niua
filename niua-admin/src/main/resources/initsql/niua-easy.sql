/*
 Navicat Premium Data Transfer

 Source Server         : 本地mysql5.7
 Source Server Type    : MySQL
 Source Server Version : 50738
 Source Host           : localhost:3306
 Source Schema         : niua-easy

 Target Server Type    : MySQL
 Target Server Version : 50738
 File Encoding         : 65001

 Date: 15/02/2023 17:36:03
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers` (
  `SCHED_NAME` varchar(120) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_NAME` varchar(200) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_GROUP` varchar(200) COLLATE utf8mb4_bin NOT NULL,
  `BLOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`) USING BTREE,
  KEY `SCHED_NAME` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars` (
  `SCHED_NAME` varchar(120) COLLATE utf8mb4_bin NOT NULL,
  `CALENDAR_NAME` varchar(200) COLLATE utf8mb4_bin NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`CALENDAR_NAME`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers` (
  `SCHED_NAME` varchar(120) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_NAME` varchar(200) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_GROUP` varchar(200) COLLATE utf8mb4_bin NOT NULL,
  `CRON_EXPRESSION` varchar(120) COLLATE utf8mb4_bin NOT NULL,
  `TIME_ZONE_ID` varchar(80) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
BEGIN;
INSERT INTO `qrtz_cron_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`, `CRON_EXPRESSION`, `TIME_ZONE_ID`) VALUES ('quartzScheduler', 'trigger测试任务', '测试分组1', '3/5 * * * * ? *', 'Asia/Shanghai');
COMMIT;

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers` (
  `SCHED_NAME` varchar(120) COLLATE utf8mb4_bin NOT NULL,
  `ENTRY_ID` varchar(95) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_NAME` varchar(200) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_GROUP` varchar(200) COLLATE utf8mb4_bin NOT NULL,
  `INSTANCE_NAME` varchar(200) COLLATE utf8mb4_bin NOT NULL,
  `FIRED_TIME` bigint(20) NOT NULL,
  `SCHED_TIME` bigint(20) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) COLLATE utf8mb4_bin NOT NULL,
  `JOB_NAME` varchar(200) COLLATE utf8mb4_bin DEFAULT NULL,
  `JOB_GROUP` varchar(200) COLLATE utf8mb4_bin DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) COLLATE utf8mb4_bin DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`ENTRY_ID`) USING BTREE,
  KEY `IDX_QRTZ_FT_TRIG_INST_NAME` (`SCHED_NAME`,`INSTANCE_NAME`) USING BTREE,
  KEY `IDX_QRTZ_FT_INST_JOB_REQ_RCVRY` (`SCHED_NAME`,`INSTANCE_NAME`,`REQUESTS_RECOVERY`) USING BTREE,
  KEY `IDX_QRTZ_FT_J_G` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`) USING BTREE,
  KEY `IDX_QRTZ_FT_JG` (`SCHED_NAME`,`JOB_GROUP`) USING BTREE,
  KEY `IDX_QRTZ_FT_T_G` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`) USING BTREE,
  KEY `IDX_QRTZ_FT_TG` (`SCHED_NAME`,`TRIGGER_GROUP`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details` (
  `SCHED_NAME` varchar(120) COLLATE utf8mb4_bin NOT NULL,
  `JOB_NAME` varchar(200) COLLATE utf8mb4_bin NOT NULL COMMENT '任务名称',
  `JOB_GROUP` varchar(200) COLLATE utf8mb4_bin NOT NULL COMMENT '任务分组',
  `DESCRIPTION` varchar(250) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '任务描述',
  `JOB_CLASS_NAME` varchar(250) COLLATE utf8mb4_bin NOT NULL COMMENT '执行类',
  `IS_DURABLE` varchar(1) COLLATE utf8mb4_bin NOT NULL,
  `IS_NONCONCURRENT` varchar(1) COLLATE utf8mb4_bin NOT NULL,
  `IS_UPDATE_DATA` varchar(1) COLLATE utf8mb4_bin NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) COLLATE utf8mb4_bin NOT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`) USING BTREE,
  KEY `IDX_QRTZ_J_REQ_RECOVERY` (`SCHED_NAME`,`REQUESTS_RECOVERY`) USING BTREE,
  KEY `IDX_QRTZ_J_GRP` (`SCHED_NAME`,`JOB_GROUP`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
BEGIN;
INSERT INTO `qrtz_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`, `DESCRIPTION`, `JOB_CLASS_NAME`, `IS_DURABLE`, `IS_NONCONCURRENT`, `IS_UPDATE_DATA`, `REQUESTS_RECOVERY`, `JOB_DATA`) VALUES ('quartzScheduler', '测试任务', '测试分组1', '测试描述', 'tech.niua.quartz.jobs.TestJob', '0', '0', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C770800000010000000017400157363686564756C6572496E7374616E63654E616D6574000F71756172747A5363686564756C65727800);
COMMIT;

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks` (
  `SCHED_NAME` varchar(120) COLLATE utf8mb4_bin NOT NULL,
  `LOCK_NAME` varchar(40) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`LOCK_NAME`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
BEGIN;
INSERT INTO `qrtz_locks` (`SCHED_NAME`, `LOCK_NAME`) VALUES ('quartzScheduler', 'TRIGGER_ACCESS');
COMMIT;

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps` (
  `SCHED_NAME` varchar(120) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_GROUP` varchar(200) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_GROUP`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state` (
  `SCHED_NAME` varchar(120) COLLATE utf8mb4_bin NOT NULL,
  `INSTANCE_NAME` varchar(200) COLLATE utf8mb4_bin NOT NULL,
  `LAST_CHECKIN_TIME` bigint(20) NOT NULL,
  `CHECKIN_INTERVAL` bigint(20) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`INSTANCE_NAME`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers` (
  `SCHED_NAME` varchar(120) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_NAME` varchar(200) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_GROUP` varchar(200) COLLATE utf8mb4_bin NOT NULL,
  `REPEAT_COUNT` bigint(20) NOT NULL,
  `REPEAT_INTERVAL` bigint(20) NOT NULL,
  `TIMES_TRIGGERED` bigint(20) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers` (
  `SCHED_NAME` varchar(120) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_NAME` varchar(200) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_GROUP` varchar(200) COLLATE utf8mb4_bin NOT NULL,
  `STR_PROP_1` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL,
  `STR_PROP_2` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL,
  `STR_PROP_3` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL,
  `INT_PROP_1` int(11) DEFAULT NULL,
  `INT_PROP_2` int(11) DEFAULT NULL,
  `LONG_PROP_1` bigint(20) DEFAULT NULL,
  `LONG_PROP_2` bigint(20) DEFAULT NULL,
  `DEC_PROP_1` decimal(13,4) DEFAULT NULL,
  `DEC_PROP_2` decimal(13,4) DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) COLLATE utf8mb4_bin DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers` (
  `SCHED_NAME` varchar(120) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_NAME` varchar(200) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_GROUP` varchar(200) COLLATE utf8mb4_bin NOT NULL,
  `JOB_NAME` varchar(200) COLLATE utf8mb4_bin NOT NULL,
  `JOB_GROUP` varchar(200) COLLATE utf8mb4_bin NOT NULL,
  `DESCRIPTION` varchar(250) COLLATE utf8mb4_bin DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(20) DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(20) DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_TYPE` varchar(8) COLLATE utf8mb4_bin NOT NULL,
  `START_TIME` bigint(20) NOT NULL,
  `END_TIME` bigint(20) DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) COLLATE utf8mb4_bin DEFAULT NULL,
  `MISFIRE_INSTR` smallint(6) DEFAULT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`) USING BTREE,
  KEY `IDX_QRTZ_T_J` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`) USING BTREE,
  KEY `IDX_QRTZ_T_JG` (`SCHED_NAME`,`JOB_GROUP`) USING BTREE,
  KEY `IDX_QRTZ_T_C` (`SCHED_NAME`,`CALENDAR_NAME`) USING BTREE,
  KEY `IDX_QRTZ_T_G` (`SCHED_NAME`,`TRIGGER_GROUP`) USING BTREE,
  KEY `IDX_QRTZ_T_STATE` (`SCHED_NAME`,`TRIGGER_STATE`) USING BTREE,
  KEY `IDX_QRTZ_T_N_STATE` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`) USING BTREE,
  KEY `IDX_QRTZ_T_N_G_STATE` (`SCHED_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`) USING BTREE,
  KEY `IDX_QRTZ_T_NEXT_FIRE_TIME` (`SCHED_NAME`,`NEXT_FIRE_TIME`) USING BTREE,
  KEY `IDX_QRTZ_T_NFT_ST` (`SCHED_NAME`,`TRIGGER_STATE`,`NEXT_FIRE_TIME`) USING BTREE,
  KEY `IDX_QRTZ_T_NFT_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`) USING BTREE,
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_STATE`) USING BTREE,
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE_GRP` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_GROUP`,`TRIGGER_STATE`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `qrtz_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
BEGIN;
INSERT INTO `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`, `JOB_NAME`, `JOB_GROUP`, `DESCRIPTION`, `NEXT_FIRE_TIME`, `PREV_FIRE_TIME`, `PRIORITY`, `TRIGGER_STATE`, `TRIGGER_TYPE`, `START_TIME`, `END_TIME`, `CALENDAR_NAME`, `MISFIRE_INSTR`, `JOB_DATA`) VALUES ('quartzScheduler', 'trigger测试任务', '测试分组1', '测试任务', '测试分组1', NULL, 1676448158000, 1676448153000, 5, 'PAUSED', 'CRON', 1617179649000, 0, NULL, 0, '');
COMMIT;

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父部门id',
  `ancestors` varchar(50) DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) DEFAULT '' COMMENT '部门名称',
  `order_num` int(11) DEFAULT '0' COMMENT '显示顺序',
  `leader` varchar(20) DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `dept_status` int(11) DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `delete_flag` int(11) DEFAULT '0' COMMENT '删除标志（0代表存在 1代表删除）',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='部门表';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
BEGIN;
INSERT INTO `sys_dept` (`id`, `parent_id`, `ancestors`, `dept_name`, `order_num`, `leader`, `phone`, `email`, `dept_status`, `delete_flag`, `create_time`, `update_time`) VALUES (100, 0, '0', '桃李科技-做软件更专业', 0, '牛蛙', '15888888888', 'ry@qq.com', 0, 0, '2022-08-06 10:15:34', '2023-02-15 17:32:37');
INSERT INTO `sys_dept` (`id`, `parent_id`, `ancestors`, `dept_name`, `order_num`, `leader`, `phone`, `email`, `dept_status`, `delete_flag`, `create_time`, `update_time`) VALUES (101, 100, '0,100', '北京总公司', 1, '牛蛙', '15888888888', 'ry@qq.com', 0, 0, '2022-08-06 10:15:34', NULL);
INSERT INTO `sys_dept` (`id`, `parent_id`, `ancestors`, `dept_name`, `order_num`, `leader`, `phone`, `email`, `dept_status`, `delete_flag`, `create_time`, `update_time`) VALUES (102, 100, '0,100', '保定分公司', 2, '牛蛙', '15888888888', 'ry@qq.com', 0, 0, '2022-08-06 10:15:34', NULL);
INSERT INTO `sys_dept` (`id`, `parent_id`, `ancestors`, `dept_name`, `order_num`, `leader`, `phone`, `email`, `dept_status`, `delete_flag`, `create_time`, `update_time`) VALUES (103, 101, '0,100,101', '研发部门', 1, '牛蛙', '15888888888', 'ry@qq.com', 0, 0, '2022-08-06 10:15:34', NULL);
INSERT INTO `sys_dept` (`id`, `parent_id`, `ancestors`, `dept_name`, `order_num`, `leader`, `phone`, `email`, `dept_status`, `delete_flag`, `create_time`, `update_time`) VALUES (104, 102, '0,100,102', '市场部门', 2, '牛蛙', '15888888888', 'ry@qq.com', 0, 0, '2022-08-06 10:15:34', NULL);
INSERT INTO `sys_dept` (`id`, `parent_id`, `ancestors`, `dept_name`, `order_num`, `leader`, `phone`, `email`, `dept_status`, `delete_flag`, `create_time`, `update_time`) VALUES (105, 101, '0,100,101', '测试部门', 3, '牛蛙', '15888888888', 'ry@qq.com', 0, 0, '2022-08-06 10:15:34', NULL);
INSERT INTO `sys_dept` (`id`, `parent_id`, `ancestors`, `dept_name`, `order_num`, `leader`, `phone`, `email`, `dept_status`, `delete_flag`, `create_time`, `update_time`) VALUES (106, 101, '0,100,101', '财务部门', 4, '牛蛙', '15888888888', 'ry@qq.com', 0, 0, '2022-08-06 10:15:34', NULL);
INSERT INTO `sys_dept` (`id`, `parent_id`, `ancestors`, `dept_name`, `order_num`, `leader`, `phone`, `email`, `dept_status`, `delete_flag`, `create_time`, `update_time`) VALUES (107, 101, '0,100,101', '运维部门', 5, '牛蛙', '15888888888', 'ry@qq.com', 0, 0, '2022-08-06 10:15:34', NULL);
INSERT INTO `sys_dept` (`id`, `parent_id`, `ancestors`, `dept_name`, `order_num`, `leader`, `phone`, `email`, `dept_status`, `delete_flag`, `create_time`, `update_time`) VALUES (108, 102, '0,100,102', '市场部门', 1, '牛蛙', '15888888888', 'ry@qq.com', 0, 0, '2022-08-06 10:15:34', NULL);
INSERT INTO `sys_dept` (`id`, `parent_id`, `ancestors`, `dept_name`, `order_num`, `leader`, `phone`, `email`, `dept_status`, `delete_flag`, `create_time`, `update_time`) VALUES (109, 102, '0,100,102', '财务部门', 2, '牛蛙', '15888888888', 'ry@qq.com', 0, 0, '2022-08-06 10:15:34', NULL);
INSERT INTO `sys_dept` (`id`, `parent_id`, `ancestors`, `dept_name`, `order_num`, `leader`, `phone`, `email`, `dept_status`, `delete_flag`, `create_time`, `update_time`) VALUES (110, 100, '0,100', '人力资源部门', 2, '牛蛙', '13555666687', '3665668556@qq.com', 0, 0, '2022-08-06 10:13:14', NULL);
INSERT INTO `sys_dept` (`id`, `parent_id`, `ancestors`, `dept_name`, `order_num`, `leader`, `phone`, `email`, `dept_status`, `delete_flag`, `create_time`, `update_time`) VALUES (111, 101, '0,100,101', '生产技术部门', 1, '牛蛙', '15555555555', '55555@qq.com', 0, 0, '2022-08-06 10:15:34', NULL);
INSERT INTO `sys_dept` (`id`, `parent_id`, `ancestors`, `dept_name`, `order_num`, `leader`, `phone`, `email`, `dept_status`, `delete_flag`, `create_time`, `update_time`) VALUES (112, 102, '0,100,102', '计划销售部门', 0, '牛蛙', '15555555555', '5555@qq.com', 0, 0, '2022-08-06 10:15:34', NULL);
INSERT INTO `sys_dept` (`id`, `parent_id`, `ancestors`, `dept_name`, `order_num`, `leader`, `phone`, `email`, `dept_status`, `delete_flag`, `create_time`, `update_time`) VALUES (113, 101, '0,100,101', '安全检查部门', 0, '牛蛙', '14444444444', '22@qq.com', 0, 0, '2022-08-06 10:15:34', NULL);
COMMIT;

-- ----------------------------
-- Table structure for sys_generator
-- ----------------------------
DROP TABLE IF EXISTS `sys_generator`;
CREATE TABLE `sys_generator` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(80) DEFAULT NULL COMMENT '模块名称',
  `table_name` varchar(80) DEFAULT NULL COMMENT '表名称',
  `ignore_flag` int(11) DEFAULT NULL COMMENT '是否忽略前缀1：是',
  `ignore_prefix` varchar(20) DEFAULT NULL COMMENT '前缀',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_generator
-- ----------------------------
BEGIN;
INSERT INTO `sys_generator` (`id`, `module_name`, `table_name`, `ignore_flag`, `ignore_prefix`, `update_time`, `create_time`) VALUES (22, 'test', 't_test', 1, 't_', '2021-01-27 22:37:22', '2021-01-21 15:17:39');
INSERT INTO `sys_generator` (`id`, `module_name`, `table_name`, `ignore_flag`, `ignore_prefix`, `update_time`, `create_time`) VALUES (25, 'operlog', 'sys_oper_log', 0, '', '2021-01-29 20:42:49', '2021-01-29 20:42:49');
INSERT INTO `sys_generator` (`id`, `module_name`, `table_name`, `ignore_flag`, `ignore_prefix`, `update_time`, `create_time`) VALUES (26, '部门管理', 'sys_dept', 1, 'sys_', '2022-08-05 16:53:04', '2022-08-05 16:53:04');
INSERT INTO `sys_generator` (`id`, `module_name`, `table_name`, `ignore_flag`, `ignore_prefix`, `update_time`, `create_time`) VALUES (27, 'score', 't_score', 1, 't_', '2022-09-18 16:56:41', '2022-09-18 16:56:41');
COMMIT;

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `operation` varchar(50) CHARACTER SET utf8 DEFAULT '' COMMENT '操作',
  `business_type` varchar(20) COLLATE utf8mb4_bin DEFAULT '0' COMMENT '业务类型',
  `method` varchar(255) CHARACTER SET utf8 DEFAULT '' COMMENT '方法名称',
  `create_time` datetime DEFAULT NULL COMMENT '操作时间',
  `oper_name` varchar(80) COLLATE utf8mb4_bin DEFAULT '' COMMENT '操作用户',
  `params` text COLLATE utf8mb4_bin COMMENT '参数',
  `ip` varchar(80) COLLATE utf8mb4_bin DEFAULT '' COMMENT '请求的ip地址',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1570 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='操作日志记录';

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
BEGIN;
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1380, '查询列表', 'LIST', 'tech.niua.auth.controller.ResourceController.listResources', '2022-08-17 11:21:39', 'admin', '[{\"id\":null,\"name\":\"\",\"orderNum\":null,\"permission\":null,\"type\":null,\"url\":null,\"parentId\":1,\"available\":null,\"path\":null,\"hidden\":null,\"alwaysShow\":null,\"redirect\":null,\"component\":null,\"icon\":null,\"roles\":null,\"childResources\":null,\"children\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1381, '查询列表', 'LIST', 'tech.niua.auth.controller.RoleController.index', '2022-08-17 11:21:43', 'admin', '[{\"id\":null,\"available\":null,\"description\":null,\"name\":\"\",\"resources\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1382, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:21:47', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":null,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1383, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:21:57', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":100,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1384, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:21:57', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":101,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1385, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:21:58', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":113,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1386, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:21:59', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":103,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1387, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:22:00', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":105,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1388, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:22:00', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":106,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1389, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:22:01', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":102,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1390, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:22:02', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":112,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1391, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:22:03', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":104,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1392, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:22:04', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":110,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1393, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:22:06', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":100,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1394, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:22:06', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":101,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1395, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:22:07', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":113,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1396, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:22:07', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":103,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1397, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:22:08', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":111,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1398, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:22:09', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":105,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1399, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:22:09', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":106,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1400, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:22:10', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":107,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1401, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:22:11', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":112,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1402, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:22:13', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":104,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1403, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:22:13', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":109,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1404, '查询任务列表', 'LIST', 'tech.niua.quartz.controller.JobController.list', '2022-08-17 11:22:50', 'admin', '[{\"jobName\":null,\"jobGroup\":null,\"description\":null,\"jobClassName\":null,\"cronExpression\":null,\"triggerName\":null,\"triggerState\":null,\"oldJobName\":null,\"oldJobGroup\":null,\"jobDataParam\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1405, '代码生成列表', 'LIST', 'tech.niua.gen.controller.GeneratorController.pageListQuery', '2022-08-17 11:22:52', 'admin', '[{\"id\":null,\"moduleName\":null,\"tableName\":\"\",\"ignoreFlag\":0,\"ignorePrefix\":null,\"createTime\":null,\"updateTime\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1406, '代码生成列表', 'LIST', 'tech.niua.gen.controller.GeneratorController.pageListQuery', '2022-08-17 11:23:04', 'admin', '[{\"id\":null,\"moduleName\":null,\"tableName\":\"\",\"ignoreFlag\":0,\"ignorePrefix\":null,\"createTime\":null,\"updateTime\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1407, '代码生成列表', 'LIST', 'tech.niua.gen.controller.GeneratorController.pageListQuery', '2022-08-17 11:54:48', 'admin', '[{\"id\":null,\"moduleName\":null,\"tableName\":\"\",\"ignoreFlag\":0,\"ignorePrefix\":null,\"createTime\":null,\"updateTime\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1408, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:55:01', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":null,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1409, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:55:04', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":101,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1410, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:55:05', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":100,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1411, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:55:06', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":113,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1412, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:55:07', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":111,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1413, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:55:08', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":107,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1414, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:55:09', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":112,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1415, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:55:10', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":108,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1416, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:55:10', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":104,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1417, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:55:11', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":110,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1418, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:55:12', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":101,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1419, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:55:12', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":100,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1420, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:55:54', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":null,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1421, '更新或添加操作', 'INSERTORUPDATE', 'tech.niua.auth.controller.UserController.saveOrUpdate', '2022-08-17 11:56:04', 'admin', '[{\"id\":0,\"username\":\"wang\",\"password\":null,\"available\":0,\"sexType\":0,\"avatar\":\"\",\"email\":\"\",\"tel\":\"</input><img src=1 onerror=alert1>\",\"deptId\":113,\"dept\":null,\"createTime\":{\"monthValue\":8,\"dayOfMonth\":17,\"dayOfWeek\":\"WEDNESDAY\",\"year\":2022,\"month\":\"AUGUST\",\"dayOfYear\":229,\"hour\":11,\"minute\":56,\"second\":3,\"nano\":677000000,\"chronology\":{\"calendarType\":\"iso8601\",\"id\":\"ISO\"}},\"updateTime\":{\"monthValue\":8,\"dayOfMonth\":17,\"dayOfWeek\":\"WEDNESDAY\",\"year\":2022,\"month\":\"AUGUST\",\"dayOfYear\":229,\"hour\":11,\"minute\":56,\"second\":3,\"nano\":677000000,\"chronology\":{\"calendarType\":\"iso8601\",\"id\":\"ISO\"}},\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null}]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1422, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:56:04', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":null,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1423, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:56:06', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":null,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1424, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:56:10', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":null,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1425, '删除操作', 'DELETE', 'tech.niua.auth.controller.UserController.delete', '2022-08-17 11:56:12', 'admin', '[[5]]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1426, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:56:12', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":null,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1427, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:56:14', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":null,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1428, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:56:27', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":null,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1429, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:56:28', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":null,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1430, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:56:29', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":null,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1431, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:56:30', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":null,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1432, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:56:40', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":102,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1433, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:56:41', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":112,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1434, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:56:41', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":112,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1435, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:56:42', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":104,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1436, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:56:43', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":110,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1437, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:56:43', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":109,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1438, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:56:44', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":108,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1439, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:56:45', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":102,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1440, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 11:59:34', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":102,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1441, '查询列表', 'LIST', 'tech.niua.auth.controller.RoleController.index', '2022-08-17 12:00:33', 'admin', '[{\"id\":null,\"available\":null,\"description\":null,\"name\":\"\",\"resources\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1442, '查询列表', 'LIST', 'tech.niua.auth.controller.ResourceController.listResources', '2022-08-17 12:00:36', 'admin', '[{\"id\":null,\"name\":\"\",\"orderNum\":null,\"permission\":null,\"type\":null,\"url\":null,\"parentId\":1,\"available\":null,\"path\":null,\"hidden\":null,\"alwaysShow\":null,\"redirect\":null,\"component\":null,\"icon\":null,\"roles\":null,\"childResources\":null,\"children\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1443, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 12:01:09', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":null,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1444, '查询任务列表', 'LIST', 'tech.niua.quartz.controller.JobController.list', '2022-08-17 12:02:59', 'admin', '[{\"jobName\":null,\"jobGroup\":null,\"description\":null,\"jobClassName\":null,\"cronExpression\":null,\"triggerName\":null,\"triggerState\":null,\"oldJobName\":null,\"oldJobGroup\":null,\"jobDataParam\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1445, '代码生成列表', 'LIST', 'tech.niua.gen.controller.GeneratorController.pageListQuery', '2022-08-17 12:03:02', 'admin', '[{\"id\":null,\"moduleName\":null,\"tableName\":\"\",\"ignoreFlag\":0,\"ignorePrefix\":null,\"createTime\":null,\"updateTime\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1446, '查询列表', 'LIST', 'tech.niua.auth.controller.ResourceController.listResources', '2022-08-17 12:03:41', 'admin', '[{\"id\":null,\"name\":\"\",\"orderNum\":null,\"permission\":null,\"type\":null,\"url\":null,\"parentId\":1,\"available\":null,\"path\":null,\"hidden\":null,\"alwaysShow\":null,\"redirect\":null,\"component\":null,\"icon\":null,\"roles\":null,\"childResources\":null,\"children\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1447, '查询列表', 'LIST', 'tech.niua.auth.controller.ResourceController.listResources', '2022-08-17 12:04:14', 'admin', '[{\"id\":null,\"name\":\"\",\"orderNum\":null,\"permission\":null,\"type\":null,\"url\":null,\"parentId\":1,\"available\":null,\"path\":null,\"hidden\":null,\"alwaysShow\":null,\"redirect\":null,\"component\":null,\"icon\":null,\"roles\":null,\"childResources\":null,\"children\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1448, '查询列表', 'LIST', 'tech.niua.auth.controller.ResourceController.listResources', '2022-08-17 12:04:26', 'admin', '[{\"id\":null,\"name\":\"\",\"orderNum\":null,\"permission\":null,\"type\":null,\"url\":null,\"parentId\":1,\"available\":null,\"path\":null,\"hidden\":null,\"alwaysShow\":null,\"redirect\":null,\"component\":null,\"icon\":null,\"roles\":null,\"childResources\":null,\"children\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1449, '查询列表', 'LIST', 'tech.niua.auth.controller.RoleController.index', '2022-08-17 12:05:09', 'admin', '[{\"id\":null,\"available\":null,\"description\":null,\"name\":\"\",\"resources\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1450, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-17 12:05:17', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":0,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":null,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1451, '查询列表', 'LIST', 'tech.niua.auth.controller.ResourceController.listResources', '2022-08-24 21:18:42', 'admin', '[{\"id\":null,\"name\":\"\",\"orderNum\":null,\"permission\":null,\"type\":null,\"url\":null,\"parentId\":1,\"available\":null,\"path\":null,\"hidden\":null,\"alwaysShow\":null,\"redirect\":null,\"component\":null,\"icon\":null,\"roles\":null,\"childResources\":null,\"children\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1452, '查询任务列表', 'LIST', 'tech.niua.quartz.controller.JobController.list', '2022-08-26 15:25:24', 'admin', '[{\"jobName\":null,\"jobGroup\":null,\"description\":null,\"jobClassName\":null,\"cronExpression\":null,\"triggerName\":null,\"triggerState\":null,\"oldJobName\":null,\"oldJobGroup\":null,\"jobDataParam\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1453, '代码生成列表', 'LIST', 'tech.niua.gen.controller.GeneratorController.pageListQuery', '2022-08-26 15:25:26', 'admin', '[{\"id\":null,\"moduleName\":null,\"tableName\":\"\",\"ignoreFlag\":0,\"ignorePrefix\":null,\"createTime\":null,\"updateTime\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1454, '列表查询', 'LIST', 'tech.niua.auth.controller.DeptController.list', '2022-08-26 15:35:21', 'admin', '[{\"parentId\":null,\"ancestors\":null,\"deptName\":null,\"orderNum\":null,\"leader\":null,\"phone\":null,\"email\":null,\"deptStatus\":null,\"deleteFlag\":null,\"parentName\":null,\"children\":[],\"createTime\":null,\"updateTime\":null,\"id\":null}]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1455, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-26 15:35:36', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":null,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1456, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-26 15:35:37', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":104,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1457, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-26 15:35:38', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":108,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1458, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-26 15:35:39', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":110,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1459, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-26 15:35:39', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":101,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1460, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-26 15:35:40', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":106,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1461, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-26 15:35:41', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":111,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1462, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-26 15:35:42', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":111,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1463, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-26 15:35:43', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":103,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1464, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-26 15:35:43', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":106,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1465, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-26 15:35:44', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":101,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1466, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-26 15:35:44', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":110,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1467, '查询列表', 'LIST', 'tech.niua.auth.controller.ResourceController.listResources', '2022-08-26 15:35:49', 'admin', '[{\"id\":null,\"name\":\"\",\"orderNum\":null,\"permission\":null,\"type\":null,\"url\":null,\"parentId\":1,\"available\":null,\"path\":null,\"hidden\":null,\"alwaysShow\":null,\"redirect\":null,\"component\":null,\"icon\":null,\"roles\":null,\"childResources\":null,\"children\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1468, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-08-30 13:46:53', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":null,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1469, '列表查询', 'LIST', 'tech.niua.auth.controller.DeptController.list', '2022-08-30 13:47:27', 'admin', '[{\"parentId\":null,\"ancestors\":null,\"deptName\":null,\"orderNum\":null,\"leader\":null,\"phone\":null,\"email\":null,\"deptStatus\":null,\"deleteFlag\":null,\"parentName\":null,\"children\":[],\"createTime\":null,\"updateTime\":null,\"id\":null}]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1470, '查询列表', 'LIST', 'tech.niua.auth.controller.RoleController.index', '2022-08-30 13:48:04', 'admin', '[{\"id\":null,\"available\":null,\"description\":null,\"name\":\"\",\"resources\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1471, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-09-01 11:32:48', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":null,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1472, '查询列表', 'LIST', 'tech.niua.auth.controller.RoleController.index', '2022-09-01 11:32:48', 'admin', '[{\"id\":null,\"available\":null,\"description\":null,\"name\":\"\",\"resources\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1473, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-09-01 11:32:59', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":104,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1474, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-09-01 11:33:00', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":108,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1475, '列表查询', 'LIST', 'tech.niua.auth.controller.DeptController.list', '2022-09-01 11:33:04', 'admin', '[{\"parentId\":null,\"ancestors\":null,\"deptName\":null,\"orderNum\":null,\"leader\":null,\"phone\":null,\"email\":null,\"deptStatus\":null,\"deleteFlag\":null,\"parentName\":null,\"children\":[],\"createTime\":null,\"updateTime\":null,\"id\":null}]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1476, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-09-01 11:33:37', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":102,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1477, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-09-01 11:33:38', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":100,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1478, '查询列表', 'LIST', 'tech.niua.auth.controller.ResourceController.listResources', '2022-09-01 11:33:56', 'admin', '[{\"id\":null,\"name\":\"\",\"orderNum\":null,\"permission\":null,\"type\":null,\"url\":null,\"parentId\":1,\"available\":null,\"path\":null,\"hidden\":null,\"alwaysShow\":null,\"redirect\":null,\"component\":null,\"icon\":null,\"roles\":null,\"childResources\":null,\"children\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1479, '查询列表', 'LIST', 'tech.niua.auth.controller.ResourceController.listResources', '2022-09-01 11:35:39', 'admin', '[{\"id\":null,\"name\":\"\",\"orderNum\":null,\"permission\":null,\"type\":null,\"url\":null,\"parentId\":1,\"available\":null,\"path\":null,\"hidden\":null,\"alwaysShow\":null,\"redirect\":null,\"component\":null,\"icon\":null,\"roles\":null,\"childResources\":null,\"children\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1480, '查询列表', 'LIST', 'tech.niua.auth.controller.RoleController.index', '2022-09-01 11:55:54', 'admin', '[{\"id\":null,\"available\":null,\"description\":null,\"name\":\"\",\"resources\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1481, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-09-01 11:55:56', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":null,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1482, '列表查询', 'LIST', 'tech.niua.auth.controller.DeptController.list', '2022-09-01 11:56:01', 'admin', '[{\"parentId\":null,\"ancestors\":null,\"deptName\":null,\"orderNum\":null,\"leader\":null,\"phone\":null,\"email\":null,\"deptStatus\":null,\"deleteFlag\":null,\"parentName\":null,\"children\":[],\"createTime\":null,\"updateTime\":null,\"id\":null}]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1483, '代码生成列表', 'LIST', 'tech.niua.gen.controller.GeneratorController.pageListQuery', '2022-09-01 12:18:51', 'admin', '[{\"id\":null,\"moduleName\":null,\"tableName\":\"\",\"ignoreFlag\":0,\"ignorePrefix\":null,\"createTime\":null,\"updateTime\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1484, '查询任务列表', 'LIST', 'tech.niua.quartz.controller.JobController.list', '2022-09-01 12:18:53', 'admin', '[{\"jobName\":null,\"jobGroup\":null,\"description\":null,\"jobClassName\":null,\"cronExpression\":null,\"triggerName\":null,\"triggerState\":null,\"oldJobName\":null,\"oldJobGroup\":null,\"jobDataParam\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1485, '查询列表', 'LIST', 'tech.niua.auth.controller.ResourceController.listResources', '2022-09-01 12:18:55', 'admin', '[{\"id\":null,\"name\":\"\",\"orderNum\":null,\"permission\":null,\"type\":null,\"url\":null,\"parentId\":1,\"available\":null,\"path\":null,\"hidden\":null,\"alwaysShow\":null,\"redirect\":null,\"component\":null,\"icon\":null,\"roles\":null,\"childResources\":null,\"children\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1486, '查询列表', 'LIST', 'tech.niua.auth.controller.ResourceController.listResources', '2022-09-18 16:44:20', 'admin', '[{\"id\":null,\"name\":\"\",\"orderNum\":null,\"permission\":null,\"type\":null,\"url\":null,\"parentId\":1,\"available\":null,\"path\":null,\"hidden\":null,\"alwaysShow\":null,\"redirect\":null,\"component\":null,\"icon\":null,\"roles\":null,\"childResources\":null,\"children\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1487, '查询列表', 'LIST', 'tech.niua.auth.controller.RoleController.index', '2022-09-18 16:44:33', 'admin', '[{\"id\":null,\"available\":null,\"description\":null,\"name\":\"\",\"resources\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1488, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-09-18 16:44:36', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":null,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1489, '列表查询', 'LIST', 'tech.niua.auth.controller.DeptController.list', '2022-09-18 16:44:40', 'admin', '[{\"parentId\":null,\"ancestors\":null,\"deptName\":null,\"orderNum\":null,\"leader\":null,\"phone\":null,\"email\":null,\"deptStatus\":null,\"deleteFlag\":null,\"parentName\":null,\"children\":[],\"createTime\":null,\"updateTime\":null,\"id\":null}]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1490, '查询任务列表', 'LIST', 'tech.niua.quartz.controller.JobController.list', '2022-09-18 16:45:06', 'admin', '[{\"jobName\":null,\"jobGroup\":null,\"description\":null,\"jobClassName\":null,\"cronExpression\":null,\"triggerName\":null,\"triggerState\":null,\"oldJobName\":null,\"oldJobGroup\":null,\"jobDataParam\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1491, '代码生成列表', 'LIST', 'tech.niua.gen.controller.GeneratorController.pageListQuery', '2022-09-18 16:45:26', 'admin', '[{\"id\":null,\"moduleName\":null,\"tableName\":\"\",\"ignoreFlag\":0,\"ignorePrefix\":null,\"createTime\":null,\"updateTime\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1492, '代码生成列表', 'LIST', 'tech.niua.gen.controller.GeneratorController.pageListQuery', '2022-09-18 16:51:37', 'admin', '[{\"id\":null,\"moduleName\":null,\"tableName\":\"\",\"ignoreFlag\":0,\"ignorePrefix\":null,\"createTime\":null,\"updateTime\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1493, '代码生成列表', 'LIST', 'tech.niua.gen.controller.GeneratorController.pageListQuery', '2022-09-18 16:56:11', 'admin', '[{\"id\":null,\"moduleName\":null,\"tableName\":\"\",\"ignoreFlag\":0,\"ignorePrefix\":null,\"createTime\":null,\"updateTime\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1494, '保存或修改数据', 'INSERTORUPDATE', 'tech.niua.gen.controller.GeneratorController.saveOrUpdate', '2022-09-18 16:56:41', 'admin', '[{\"id\":27,\"moduleName\":\"score\",\"tableName\":\"t_score\",\"ignoreFlag\":1,\"ignorePrefix\":\"t_\",\"createTime\":{\"hour\":16,\"minute\":56,\"second\":41,\"nano\":205000000,\"dayOfYear\":261,\"dayOfMonth\":18,\"dayOfWeek\":\"SUNDAY\",\"year\":2022,\"month\":\"SEPTEMBER\",\"monthValue\":9,\"chronology\":{\"id\":\"ISO\",\"calendarType\":\"iso8601\"}},\"updateTime\":{\"hour\":16,\"minute\":56,\"second\":41,\"nano\":205000000,\"dayOfYear\":261,\"dayOfMonth\":18,\"dayOfWeek\":\"SUNDAY\",\"year\":2022,\"month\":\"SEPTEMBER\",\"monthValue\":9,\"chronology\":{\"id\":\"ISO\",\"calendarType\":\"iso8601\"}}}]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1495, '代码生成列表', 'LIST', 'tech.niua.gen.controller.GeneratorController.pageListQuery', '2022-09-18 16:56:41', 'admin', '[{\"id\":null,\"moduleName\":null,\"tableName\":\"\",\"ignoreFlag\":0,\"ignorePrefix\":null,\"createTime\":null,\"updateTime\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1496, '生成代码', 'GENCODE', 'tech.niua.gen.controller.GeneratorController.autoGenerator', '2022-09-18 16:56:53', 'admin', '[27]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1497, '代码生成列表', 'LIST', 'tech.niua.gen.controller.GeneratorController.pageListQuery', '2022-10-11 15:52:10', 'admin', '[{\"id\":null,\"moduleName\":null,\"tableName\":\"\",\"ignoreFlag\":0,\"ignorePrefix\":null,\"createTime\":null,\"updateTime\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1498, '查询列表', 'LIST', 'tech.niua.auth.controller.ResourceController.listResources', '2022-10-11 15:53:40', 'admin', '[{\"id\":null,\"name\":\"\",\"orderNum\":null,\"permission\":null,\"type\":null,\"url\":null,\"parentId\":1,\"available\":null,\"path\":null,\"hidden\":null,\"alwaysShow\":null,\"redirect\":null,\"component\":null,\"icon\":null,\"roles\":null,\"childResources\":null,\"children\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1499, '查询列表', 'LIST', 'tech.niua.auth.controller.RoleController.index', '2022-10-11 15:53:40', 'admin', '[{\"id\":null,\"available\":null,\"description\":null,\"name\":\"\",\"resources\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1500, '添加或编辑操作', 'INSERTORUPDATE', 'tech.niua.auth.controller.RoleController.saveOrUpdate', '2022-10-11 15:53:59', 'admin', '[{\"id\":0,\"available\":true,\"description\":\"ddd\",\"name\":\"1111\",\"resources\":null}]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1501, '查询列表', 'LIST', 'tech.niua.auth.controller.RoleController.index', '2022-10-11 15:53:59', 'admin', '[{\"id\":null,\"available\":null,\"description\":null,\"name\":\"\",\"resources\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1502, '删除操作', 'DELETE', 'tech.niua.auth.controller.RoleController.delete', '2022-10-11 15:55:12', 'admin', '[[2]]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1503, '查询列表', 'LIST', 'tech.niua.auth.controller.RoleController.index', '2022-10-11 15:55:12', 'admin', '[{\"id\":null,\"available\":null,\"description\":null,\"name\":\"\",\"resources\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1504, '查询列表', 'LIST', 'tech.niua.auth.controller.RoleController.index', '2022-10-11 15:58:04', 'admin', '[{\"id\":null,\"available\":null,\"description\":null,\"name\":\"\",\"resources\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1505, '添加或编辑操作', 'INSERTORUPDATE', 'tech.niua.auth.controller.RoleController.saveOrUpdate', '2022-10-11 15:58:35', 'admin', '[{\"id\":0,\"available\":true,\"description\":\"<script>alert(1)</script>\",\"name\":\"测试\",\"resources\":null}]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1506, '查询列表', 'LIST', 'tech.niua.auth.controller.RoleController.index', '2022-10-11 15:58:36', 'admin', '[{\"id\":null,\"available\":null,\"description\":null,\"name\":\"\",\"resources\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1507, '查询列表', 'LIST', 'tech.niua.auth.controller.RoleController.index', '2022-10-11 15:58:43', 'admin', '[{\"id\":null,\"available\":null,\"description\":null,\"name\":\"\",\"resources\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1508, '查询列表', 'LIST', 'tech.niua.auth.controller.RoleController.index', '2022-10-11 15:59:41', 'admin', '[{\"id\":null,\"available\":null,\"description\":null,\"name\":\"\",\"resources\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1509, '添加或编辑操作', 'INSERTORUPDATE', 'tech.niua.auth.controller.RoleController.saveOrUpdate', '2022-10-11 15:59:44', 'admin', '[{\"id\":3,\"available\":true,\"description\":\"\",\"name\":\"测试\",\"resources\":null}]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1510, '查询列表', 'LIST', 'tech.niua.auth.controller.RoleController.index', '2022-10-11 15:59:44', 'admin', '[{\"id\":null,\"available\":null,\"description\":null,\"name\":\"\",\"resources\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1511, '查询列表', 'LIST', 'tech.niua.auth.controller.RoleController.index', '2022-10-11 15:59:46', 'admin', '[{\"id\":null,\"available\":null,\"description\":null,\"name\":\"\",\"resources\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1512, '添加或编辑操作', 'INSERTORUPDATE', 'tech.niua.auth.controller.RoleController.saveOrUpdate', '2022-10-11 16:00:06', 'admin', '[{\"id\":3,\"available\":true,\"description\":\"ddd\",\"name\":\"测试\",\"resources\":null}]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1513, '查询列表', 'LIST', 'tech.niua.auth.controller.RoleController.index', '2022-10-11 16:00:06', 'admin', '[{\"id\":null,\"available\":null,\"description\":null,\"name\":\"\",\"resources\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1514, '查询列表', 'LIST', 'tech.niua.auth.controller.RoleController.index', '2022-10-11 16:02:25', 'admin', '[{\"id\":null,\"available\":null,\"description\":null,\"name\":\"\",\"resources\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1515, '添加或编辑操作', 'INSERTORUPDATE', 'tech.niua.auth.controller.RoleController.saveOrUpdate', '2022-10-11 16:02:44', 'admin', '[{\"id\":3,\"available\":true,\"description\":\"ddd<script>dsadfasd</script>\",\"name\":\"测试\",\"resources\":null}]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1516, '查询列表', 'LIST', 'tech.niua.auth.controller.RoleController.index', '2022-10-11 16:02:44', 'admin', '[{\"id\":null,\"available\":null,\"description\":null,\"name\":\"\",\"resources\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1517, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-10-11 16:02:51', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":null,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1518, '查询列表', 'LIST', 'tech.niua.auth.controller.RoleController.index', '2022-10-11 16:04:01', 'admin', '[{\"id\":null,\"available\":null,\"description\":null,\"name\":\"\",\"resources\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1519, '添加或编辑操作', 'INSERTORUPDATE', 'tech.niua.auth.controller.RoleController.saveOrUpdate', '2022-10-11 16:04:05', 'admin', '[{\"id\":3,\"available\":true,\"description\":\"ddd\",\"name\":\"测试\",\"resources\":null}]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1520, '查询列表', 'LIST', 'tech.niua.auth.controller.RoleController.index', '2022-10-11 16:04:05', 'admin', '[{\"id\":null,\"available\":null,\"description\":null,\"name\":\"\",\"resources\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1521, '删除操作', 'DELETE', 'tech.niua.auth.controller.RoleController.delete', '2022-10-11 16:04:34', 'admin', '[[3]]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1522, '查询列表', 'LIST', 'tech.niua.auth.controller.RoleController.index', '2022-10-11 16:04:34', 'admin', '[{\"id\":null,\"available\":null,\"description\":null,\"name\":\"\",\"resources\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1523, '查询列表', 'LIST', 'tech.niua.auth.controller.ResourceController.listResources', '2022-10-11 17:45:58', 'admin', '[{\"id\":null,\"name\":\"\",\"orderNum\":null,\"permission\":null,\"type\":null,\"url\":null,\"parentId\":1,\"available\":null,\"path\":null,\"hidden\":null,\"alwaysShow\":null,\"redirect\":null,\"component\":null,\"icon\":null,\"roles\":null,\"childResources\":null,\"children\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1524, '查询列表', 'LIST', 'tech.niua.auth.controller.RoleController.index', '2022-10-11 17:46:00', 'admin', '[{\"id\":null,\"available\":null,\"description\":null,\"name\":\"\",\"resources\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1525, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2022-10-11 17:46:01', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":null,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1526, '列表查询', 'LIST', 'tech.niua.auth.controller.DeptController.list', '2022-10-11 17:46:03', 'admin', '[{\"parentId\":null,\"ancestors\":null,\"deptName\":null,\"orderNum\":null,\"leader\":null,\"phone\":null,\"email\":null,\"deptStatus\":null,\"deleteFlag\":null,\"parentName\":null,\"children\":[],\"createTime\":null,\"updateTime\":null,\"id\":null}]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1527, '查询任务列表', 'LIST', 'tech.niua.quartz.controller.JobController.list', '2022-10-11 17:46:06', 'admin', '[{\"jobName\":null,\"jobGroup\":null,\"description\":null,\"jobClassName\":null,\"cronExpression\":null,\"triggerName\":null,\"triggerState\":null,\"oldJobName\":null,\"oldJobGroup\":null,\"jobDataParam\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1528, '代码生成列表', 'LIST', 'tech.niua.gen.controller.GeneratorController.pageListQuery', '2022-10-11 17:46:07', 'admin', '[{\"id\":null,\"moduleName\":null,\"tableName\":\"\",\"ignoreFlag\":0,\"ignorePrefix\":null,\"createTime\":null,\"updateTime\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1529, '添加或编辑操作', 'INSERTORUPDATE', 'tech.niua.auth.controller.RoleController.saveOrUpdate', '2022-10-11 17:46:25', 'admin', '[{\"id\":0,\"available\":true,\"description\":\"11\",\"name\":\"11\",\"resources\":null}]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1530, '查询列表', 'LIST', 'tech.niua.auth.controller.RoleController.index', '2022-10-11 17:46:25', 'admin', '[{\"id\":null,\"available\":null,\"description\":null,\"name\":\"\",\"resources\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1531, '删除操作', 'DELETE', 'tech.niua.auth.controller.RoleController.delete', '2022-10-11 17:46:28', 'admin', '[[4]]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1532, '查询列表', 'LIST', 'tech.niua.auth.controller.RoleController.index', '2022-10-11 17:46:28', 'admin', '[{\"id\":null,\"available\":null,\"description\":null,\"name\":\"\",\"resources\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1533, '代码生成列表', 'LIST', 'tech.niua.gen.controller.GeneratorController.pageListQuery', '2022-10-21 14:46:31', 'admin', '[{\"id\":null,\"moduleName\":null,\"tableName\":\"\",\"ignoreFlag\":0,\"ignorePrefix\":null,\"createTime\":null,\"updateTime\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1534, '查询列表', 'LIST', 'tech.niua.auth.controller.ResourceController.listResources', '2022-10-21 14:46:35', 'admin', '[{\"id\":null,\"name\":\"\",\"orderNum\":null,\"permission\":null,\"type\":null,\"url\":null,\"parentId\":1,\"available\":null,\"path\":null,\"hidden\":null,\"alwaysShow\":null,\"redirect\":null,\"component\":null,\"icon\":null,\"roles\":null,\"childResources\":null,\"children\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1535, '查询任务列表', 'LIST', 'tech.niua.quartz.controller.JobController.list', '2022-10-21 14:47:51', 'admin', '[{\"jobName\":null,\"jobGroup\":null,\"description\":null,\"jobClassName\":null,\"cronExpression\":null,\"triggerName\":null,\"triggerState\":null,\"oldJobName\":null,\"oldJobGroup\":null,\"jobDataParam\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1536, '查询列表', 'LIST', 'tech.niua.auth.controller.ResourceController.listResources', '2023-02-15 16:01:36', 'admin', '[{\"id\":null,\"name\":\"\",\"orderNum\":null,\"permission\":null,\"type\":null,\"url\":null,\"parentId\":1,\"available\":null,\"path\":null,\"hidden\":null,\"alwaysShow\":null,\"redirect\":null,\"component\":null,\"icon\":null,\"roles\":null,\"childResources\":null,\"children\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1537, '查询列表', 'LIST', 'tech.niua.auth.controller.RoleController.index', '2023-02-15 16:01:37', 'admin', '[{\"id\":null,\"available\":null,\"description\":null,\"name\":\"\",\"resources\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1538, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2023-02-15 16:01:39', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":null,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1539, '查询任务列表', 'LIST', 'tech.niua.quartz.controller.JobController.list', '2023-02-15 16:01:43', 'admin', '[{\"jobName\":null,\"jobGroup\":null,\"description\":null,\"jobClassName\":null,\"cronExpression\":null,\"triggerName\":null,\"triggerState\":null,\"oldJobName\":null,\"oldJobGroup\":null,\"jobDataParam\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1540, '触发任务', 'UPDATE', 'tech.niua.quartz.controller.JobController.trigger', '2023-02-15 16:02:08', 'admin', '[{\"jobName\":\"测试任务\",\"jobGroup\":\"测试分组1\",\"description\":null,\"jobClassName\":null,\"cronExpression\":null,\"triggerName\":null,\"triggerState\":null,\"oldJobName\":null,\"oldJobGroup\":null,\"jobDataParam\":null}]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1541, '查询任务列表', 'LIST', 'tech.niua.quartz.controller.JobController.list', '2023-02-15 16:02:08', 'admin', '[{\"jobName\":null,\"jobGroup\":null,\"description\":null,\"jobClassName\":null,\"cronExpression\":null,\"triggerName\":null,\"triggerState\":null,\"oldJobName\":null,\"oldJobGroup\":null,\"jobDataParam\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1542, '暂停任务', 'UPDATE', 'tech.niua.quartz.controller.JobController.pause', '2023-02-15 16:02:37', 'admin', '[{\"jobName\":\"测试任务\",\"jobGroup\":\"测试分组1\",\"description\":null,\"jobClassName\":null,\"cronExpression\":null,\"triggerName\":null,\"triggerState\":null,\"oldJobName\":null,\"oldJobGroup\":null,\"jobDataParam\":null}]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1543, '查询任务列表', 'LIST', 'tech.niua.quartz.controller.JobController.list', '2023-02-15 16:02:37', 'admin', '[{\"jobName\":null,\"jobGroup\":null,\"description\":null,\"jobClassName\":null,\"cronExpression\":null,\"triggerName\":null,\"triggerState\":null,\"oldJobName\":null,\"oldJobGroup\":null,\"jobDataParam\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1544, '代码生成列表', 'LIST', 'tech.niua.gen.controller.GeneratorController.pageListQuery', '2023-02-15 16:02:42', 'admin', '[{\"id\":null,\"moduleName\":null,\"tableName\":\"\",\"ignoreFlag\":0,\"ignorePrefix\":null,\"createTime\":null,\"updateTime\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1545, '查询列表', 'LIST', 'tech.niua.auth.controller.ResourceController.listResources', '2023-02-15 16:49:07', 'admin', '[{\"id\":null,\"name\":\"\",\"orderNum\":null,\"permission\":null,\"type\":null,\"url\":null,\"parentId\":1,\"available\":null,\"path\":null,\"hidden\":null,\"alwaysShow\":null,\"redirect\":null,\"component\":null,\"icon\":null,\"roles\":null,\"childResources\":null,\"children\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1546, '查询列表', 'LIST', 'tech.niua.auth.controller.RoleController.index', '2023-02-15 16:49:59', 'admin', '[{\"id\":null,\"available\":null,\"description\":null,\"name\":\"\",\"resources\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1547, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2023-02-15 16:50:53', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":null,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1548, '列表查询', 'LIST', 'tech.niua.auth.controller.DeptController.list', '2023-02-15 16:52:05', 'admin', '[{\"parentId\":null,\"ancestors\":null,\"deptName\":null,\"orderNum\":null,\"leader\":null,\"phone\":null,\"email\":null,\"deptStatus\":null,\"deleteFlag\":null,\"parentName\":null,\"children\":[],\"createTime\":null,\"updateTime\":null,\"id\":null}]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1549, '列表查询', 'LIST', 'tech.niua.auth.controller.DeptController.list', '2023-02-15 16:52:45', 'admin', '[{\"parentId\":null,\"ancestors\":null,\"deptName\":null,\"orderNum\":null,\"leader\":null,\"phone\":null,\"email\":null,\"deptStatus\":null,\"deleteFlag\":null,\"parentName\":null,\"children\":[],\"createTime\":null,\"updateTime\":null,\"id\":null}]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1550, '列表查询', 'LIST', 'tech.niua.auth.controller.DeptController.list', '2023-02-15 16:52:58', 'admin', '[{\"parentId\":null,\"ancestors\":null,\"deptName\":null,\"orderNum\":null,\"leader\":null,\"phone\":null,\"email\":null,\"deptStatus\":null,\"deleteFlag\":null,\"parentName\":null,\"children\":[],\"createTime\":null,\"updateTime\":null,\"id\":null}]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1551, '列表查询', 'LIST', 'tech.niua.auth.controller.DeptController.list', '2023-02-15 16:54:03', 'admin', '[{\"parentId\":null,\"ancestors\":null,\"deptName\":null,\"orderNum\":null,\"leader\":null,\"phone\":null,\"email\":null,\"deptStatus\":null,\"deleteFlag\":null,\"parentName\":null,\"children\":[],\"createTime\":null,\"updateTime\":null,\"id\":null}]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1552, '查询列表', 'LIST', 'tech.niua.auth.controller.ResourceController.listResources', '2023-02-15 16:54:42', 'admin', '[{\"id\":null,\"name\":\"\",\"orderNum\":null,\"permission\":null,\"type\":null,\"url\":null,\"parentId\":1,\"available\":null,\"path\":null,\"hidden\":null,\"alwaysShow\":null,\"redirect\":null,\"component\":null,\"icon\":null,\"roles\":null,\"childResources\":null,\"children\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1553, '查询列表', 'LIST', 'tech.niua.auth.controller.ResourceController.listResources', '2023-02-15 17:11:08', 'admin', '[{\"id\":null,\"name\":\"\",\"orderNum\":null,\"permission\":null,\"type\":null,\"url\":null,\"parentId\":1,\"available\":null,\"path\":null,\"hidden\":null,\"alwaysShow\":null,\"redirect\":null,\"component\":null,\"icon\":null,\"roles\":null,\"childResources\":null,\"children\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1554, '查询列表', 'LIST', 'tech.niua.auth.controller.RoleController.index', '2023-02-15 17:11:39', 'admin', '[{\"id\":null,\"available\":null,\"description\":null,\"name\":\"\",\"resources\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1555, '列表查询', 'LIST', 'tech.niua.auth.controller.DeptController.list', '2023-02-15 17:13:03', 'admin', '[{\"parentId\":null,\"ancestors\":null,\"deptName\":null,\"orderNum\":null,\"leader\":null,\"phone\":null,\"email\":null,\"deptStatus\":null,\"deleteFlag\":null,\"parentName\":null,\"children\":[],\"createTime\":null,\"updateTime\":null,\"id\":null}]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1556, '查询列表', 'LIST', 'tech.niua.auth.controller.ResourceController.listResources', '2023-02-15 17:13:53', 'admin', '[{\"id\":null,\"name\":\"\",\"orderNum\":null,\"permission\":null,\"type\":null,\"url\":null,\"parentId\":1,\"available\":null,\"path\":null,\"hidden\":null,\"alwaysShow\":null,\"redirect\":null,\"component\":null,\"icon\":null,\"roles\":null,\"childResources\":null,\"children\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1557, '查询列表', 'LIST', 'tech.niua.auth.controller.RoleController.index', '2023-02-15 17:15:35', 'admin', '[{\"id\":null,\"available\":null,\"description\":null,\"name\":\"\",\"resources\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1558, '列表查询', 'LIST', 'tech.niua.auth.controller.DeptController.list', '2023-02-15 17:16:32', 'admin', '[{\"parentId\":null,\"ancestors\":null,\"deptName\":null,\"orderNum\":null,\"leader\":null,\"phone\":null,\"email\":null,\"deptStatus\":null,\"deleteFlag\":null,\"parentName\":null,\"children\":[],\"createTime\":null,\"updateTime\":null,\"id\":null}]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1559, '查询列表', 'LIST', 'tech.niua.auth.controller.ResourceController.listResources', '2023-02-15 17:35:22', 'admin', '[{\"id\":null,\"name\":\"\",\"orderNum\":null,\"permission\":null,\"type\":null,\"url\":null,\"parentId\":1,\"available\":null,\"path\":null,\"hidden\":null,\"alwaysShow\":null,\"redirect\":null,\"component\":null,\"icon\":null,\"roles\":null,\"childResources\":null,\"children\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1560, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2023-02-15 17:35:39', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":null,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1561, '列表查询', 'LIST', 'tech.niua.auth.controller.DeptController.list', '2023-02-15 17:35:40', 'admin', '[{\"parentId\":null,\"ancestors\":null,\"deptName\":null,\"orderNum\":null,\"leader\":null,\"phone\":null,\"email\":null,\"deptStatus\":null,\"deleteFlag\":null,\"parentName\":null,\"children\":[],\"createTime\":null,\"updateTime\":null,\"id\":null}]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1562, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2023-02-15 17:35:43', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":112,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1563, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2023-02-15 17:35:43', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":109,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1564, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2023-02-15 17:35:44', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":104,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1565, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2023-02-15 17:35:44', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":109,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1566, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2023-02-15 17:35:45', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":102,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1567, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2023-02-15 17:35:46', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":104,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1568, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2023-02-15 17:35:47', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":100,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
INSERT INTO `sys_oper_log` (`id`, `operation`, `business_type`, `method`, `create_time`, `oper_name`, `params`, `ip`) VALUES (1569, '列表查询', 'LIST', 'tech.niua.auth.controller.UserController.index', '2023-02-15 17:35:48', 'admin', '[{\"id\":null,\"username\":\"\",\"password\":null,\"available\":null,\"sexType\":null,\"avatar\":null,\"email\":null,\"tel\":null,\"deptId\":111,\"dept\":null,\"createTime\":null,\"updateTime\":null,\"roles\":null,\"rolesIdList\":null,\"newPassword\":null,\"roleId\":null},1,10]', '127.0.0.1');
COMMIT;

-- ----------------------------
-- Table structure for sys_resource
-- ----------------------------
DROP TABLE IF EXISTS `sys_resource`;
CREATE TABLE `sys_resource` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `available` bit(1) DEFAULT b'1',
  `name` varchar(255) DEFAULT NULL,
  `order_num` int(11) NOT NULL,
  `permission` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL COMMENT 'URL地址',
  `component` varchar(255) DEFAULT NULL COMMENT '组件',
  `redirect` varchar(255) DEFAULT NULL COMMENT '重定向',
  `always_show` varchar(255) DEFAULT NULL COMMENT '总是显示',
  `hidden` bit(1) DEFAULT b'0' COMMENT '菜单是否隐藏',
  `icon` varchar(50) DEFAULT NULL COMMENT '图标',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK3fekum3ead5klp7y4lckn5ohi` (`parent_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=333 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_resource
-- ----------------------------
BEGIN;
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (1, b'1', '顶级栏目', 100, '/', 0, NULL, 0, '/top', 'layout/Layout', NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (2, b'1', '权限配置', 8, '/permissions', 0, '', 1, '/permissions', '', NULL, NULL, b'0', 'el-icon-setting');
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (3, b'1', '角色管理', 102, '/role', 0, NULL, 2, '/role', 'views/auth/Role', NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (4, b'1', '权限管理', 103, '/resource', 0, NULL, 2, '/resource', 'views/auth/Resource', NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (5, b'1', '用户管理', 100, '/user', 0, NULL, 2, '/user', 'views/auth/User', NULL, NULL, b'0', '');
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (6, b'1', '编辑', 100, '/role/editor-role', 1, NULL, 3, '/role/editor-role', NULL, NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (7, b'1', '添加权限节点', 100, '/resource/add-permission', 1, NULL, 4, '/resource/add-permission', NULL, NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (8, b'1', '添加管理员', 100, '/user/saveOrUpdate', 1, NULL, 5, '/user/saveOrUpdate', NULL, NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (9, b'1', '添加角色', 100, '/role/add-role', 1, NULL, 3, '/role/add-role', NULL, NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (10, b'1', '删除角色', 100, '/role/delete', 1, NULL, 3, '/role/delete', NULL, NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (11, b'1', '删除用户', 100, '/user/delete', 1, NULL, 5, '/user/delete', NULL, NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (12, b'1', '删除权限', 100, '/resource/delete', 1, NULL, 4, '/resource/delete', NULL, NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (13, b'1', '启用', 100, '/user/available-user', 1, NULL, 3, '/user/available-user', NULL, NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (14, b'1', '修改管理员密码', 100, '/user/modify-password', 1, NULL, 5, '/user/modify-password', NULL, NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (16, b'1', '权限编辑', 100, '/resource/saveOrUpdate', 1, NULL, 4, '/resource/saveOrUpdate', NULL, NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (150, b'1', '编辑管理员信息', 100, '/user/edit-user', 1, NULL, 5, '/user/edit-user', NULL, NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (285, b'1', '定时任务管理', 1, '/quartz', 0, '', 1, '/quartz', 'views/job/Job', NULL, NULL, b'0', 'el-icon-timer');
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (286, b'1', '查看', 1, '/quartz', 1, '', 285, '/quartz', NULL, NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (287, b'1', '保存', 1, '/quartz/add', 1, '', 285, '/quartz/add', NULL, NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (288, b'1', '删除权限', 1, '/quartz/delete', 1, '', 285, '/quartz/delete', NULL, NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (289, b'1', '导出权限', 1, '/quartz/export', 1, '', 285, '/quartz/export', NULL, NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (290, b'1', '暂停', 1, '/quartz/pause', 1, '', 285, '/quartz/pause', NULL, NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (291, b'1', '恢复任务', 1, '/quartz/resume', 1, '', 285, '/quartz/resume', NULL, NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (292, b'1', '执行任务', 1, '/quartz/trigger', 1, '', 285, '/quartz/trigger', NULL, NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (293, b'1', '操作日志', 1, '/sysOperLog', 0, '', 2, '/sysOperLog', 'views/log/SysOperLog', NULL, NULL, b'0', '');
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (294, b'1', '查看', 1, '/sysOperLog', 1, '', 293, '/sysOperLog', NULL, NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (295, b'1', '保存和编辑权限', 1, '/sysOperLog/saveOrUpdate', 1, '', 293, '/sysOperLog/saveOrUpdate', NULL, NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (296, b'1', '删除权限', 1, '/sysOperLog/delete', 1, '', 293, '/sysOperLog/delete', NULL, NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (297, b'1', '导出权限', 1, '/sysOperLog/export', 1, '', 293, '/sysOperLog/export', NULL, NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (298, b'1', '代码生成', 1, '/generator', 0, '', 1, '/generator', 'views/generator/Generator', NULL, NULL, b'0', 'code');
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (299, b'1', '查看', 1, '/generator', 1, '', 298, '/generator', NULL, NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (300, b'1', '保存和编辑权限', 1, '/generator/saveOrUpdate', 1, '', 298, '/generator/saveOrUpdate', NULL, NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (301, b'1', '删除权限', 1, '/generator/delete', 1, '', 298, '/generator/delete', NULL, NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (302, b'1', '导出权限', 1, '/generator/export', 1, '', 298, '/generator/export', NULL, NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (303, b'1', '生成代码', 1, '/generator/autoGenerator', 1, '', 298, '/generator/autoGenerator', NULL, NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (309, b'1', '用户重置密码', 1, '/user/resetPassword', 1, '', 5, '/user/resetPassword', NULL, NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (310, b'1', '首页', 100, '/index', 0, '', 1, '/index', 'views/index/Index', NULL, 'true', b'0', 'el-icon-data-line');
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (321, b'1', '部门管理', 100, '/dept', 0, '', 2, '/dept', 'views/auth/Dept', NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (322, b'1', '查看', 100, '/dept', 1, '', 321, '/dept', '', NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (323, b'1', '保存和编辑权限', 100, '/dept/saveOrUpdate', 1, '', 321, '/dept', '', NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (324, b'1', '删除权限', 100, '/dept/delete', 1, '', 321, '/dept', '', NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (325, b'1', '导出权限', 100, '/dept/export', 1, '', 321, '/dept', '', NULL, NULL, b'0', NULL);
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (326, b'1', '图表展示', 97, '/upload', 0, '', 1, '/upload', 'views/analysis/Analysis', NULL, NULL, b'0', 'chart');
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (327, b'1', 'Swagger', 99, '/swagger', 0, 'http://localhost:9527/swagger-ui.html', 329, '/swagger', '', NULL, NULL, b'0', 'swagger');
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (328, b'1', 'Druid', 98, '/druid', 0, 'http://localhost:9527/druid/index.html', 329, '/druid', '', NULL, NULL, b'0', 'druid');
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (329, b'1', '第三方服务', 96, '/thirdService', 0, '', 1, '/thirdService', '', NULL, NULL, b'0', 'component');
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (330, b'1', '上传示例', 95, '/uploadExample', 0, '', 332, '/uploadExample', 'views/commons/Upload', NULL, NULL, b'0', 'upload');
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (331, b'1', 'UEditor示例', 100, '/ueditor', 0, '', 332, '/ueditor', 'views/commons/Ueditor', NULL, NULL, b'0', 'edit');
INSERT INTO `sys_resource` (`id`, `available`, `name`, `order_num`, `permission`, `type`, `url`, `parent_id`, `path`, `component`, `redirect`, `always_show`, `hidden`, `icon`) VALUES (332, b'1', '组件示例', 93, '/example', 0, '', 1, '/example', '', NULL, NULL, b'0', 'example');
COMMIT;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `available` bit(1) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_role` (`id`, `available`, `description`, `name`) VALUES (1, b'1', '', '管理员');
COMMIT;

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色id',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='部门角色关联表';

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_role_resources
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_resources`;
CREATE TABLE `sys_role_resources` (
  `sys_role_id` bigint(20) NOT NULL,
  `resources_id` bigint(20) NOT NULL,
  KEY `FKog6jj4v6yh9e1ilxk2mwuk75a` (`resources_id`) USING BTREE,
  KEY `FKsqkqfd2hpr5cc2kbrtgoced2w` (`sys_role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_role_resources
-- ----------------------------
BEGIN;
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 1);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 285);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 286);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 287);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 288);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 289);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 290);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 291);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 292);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 298);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 299);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 300);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 301);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 302);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 303);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 2);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 293);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 294);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 295);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 296);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 297);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 5);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 309);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 8);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 11);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 14);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 150);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 321);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 322);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 323);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 324);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 325);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 3);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 6);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 9);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 10);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 13);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 4);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 7);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 12);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 16);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 332);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 330);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 331);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 329);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 328);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 327);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 326);
INSERT INTO `sys_role_resources` (`sys_role_id`, `resources_id`) VALUES (1, 310);
COMMIT;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL COMMENT '头像',
  `available` int(11) DEFAULT '0' COMMENT '是否可用：0可用，1不可用',
  `email` varchar(255) DEFAULT NULL,
  `tel` varchar(255) DEFAULT NULL,
  `sex_type` int(11) DEFAULT NULL COMMENT '性别(0.男,1.女)',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门id',
  `delete_flag` int(11) DEFAULT '0' COMMENT '是否删除：0未删除，1已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
BEGIN;
INSERT INTO `sys_user` (`id`, `create_time`, `password`, `update_time`, `username`, `avatar`, `available`, `email`, `tel`, `sex_type`, `dept_id`, `delete_flag`) VALUES (1, '2017-07-11 17:42:18', '$2a$10$SIU57gfkh/TsIVYALXBNAeDnQzkm652FT9cg4h8wtEfC306uliyYa', '2022-08-12 16:16:36', 'admin', 'http://127.0.0.1:9527/profile/upload/2022/08/12/159b5e03-d578-4ac2-9017-cc22899ecb01.jpg', 1, '119115556@qq.com', '15030103078', 1, 102, 0);
COMMIT;

-- ----------------------------
-- Table structure for sys_user_roles
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_roles`;
CREATE TABLE `sys_user_roles` (
  `sys_user_id` bigint(20) NOT NULL,
  `roles_id` bigint(20) NOT NULL,
  KEY `FKdpvc6d7xqpqr43dfuk1s27cqh` (`roles_id`) USING BTREE,
  KEY `FKd0ut7sloes191bygyf7a3pk52` (`sys_user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_user_roles
-- ----------------------------
BEGIN;
INSERT INTO `sys_user_roles` (`sys_user_id`, `roles_id`) VALUES (1, 1);
INSERT INTO `sys_user_roles` (`sys_user_id`, `roles_id`) VALUES (2, 38);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
