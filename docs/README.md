# 介绍

## NIUA-EASY

基于 `SpringBoot` 、 `String Security` 、 `Jwt` 、 `Mybatis-Plus` 、 `Vue` 的前后端分离的后台管理系统

> niua学习教程，架构、业务、技术要点全方位解析。
> 
> niua项目是一套后台管理系统，使用现阶段主流技术实现。

**网址信息**
- 牛蛙官网: [http://www.niua.tech](http://www.niua.tech)
- 代码下载：[Wilson/niua](https://gitee.com/wilson2016/niua)


**系统需求**
- JDK >= 1.8
- MySQL >= 5.7
- Maven >= 3.0
- Node >= 12
- Redis >= 3

**主要贡献人员（排名不分先后）**

`王震` `刘卓` `李鸿森` `白泽宇` `柳震中`