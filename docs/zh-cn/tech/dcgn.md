# 导出功能

对于数据有时需要在其他方面用到，需要在系统中将数据导出，因此在牛蛙框架中同样封装了导出功能，导出为excel；

函数主体：

```
    exportExcel: function () {
      let url = this.urls.exportExcel;
      this.axios({
        method: "GET",
        url: url,
        data: {},
      })
          .then((res) => {
            let code = res.data.code;
            if (code == 200) {
              console.log(res.data.data);
              window.location.href = process.env.VUE_APP_BASEURL + "/profile/download/" + res.data.data;
            }
          })
          .catch((error) => {
            console.log(error);
          });
    },
```

后端代码：

```
    /**
    * 数据导出
    * @return
    */
    @Log(value = "数据导出", businessType = BusinessType.EXPORT)
    @PreAuthorize("hasAuthority('/test/export')")
    @GetMapping("/export")
    public ResultJson export(Test test) {
        List<Test> list = testService.list();
        ExcelUtil<Test> util = new ExcelUtil<>(Test.class);
        return util.exportExcel(list, "自动生成数据");
    }
```

总结：数据导出不需要参数，默认导出当前表所有内容，前端向后端发起请求后，后端会将数据库所有信息查出存于`list`，再将`list`作为参数传给封装好的导出工具`ExcelUtil`的`exportExcel`方法，该方法返回的内容为生成好的excel的url，在前台使用下载链接的形式接收url，实现点击导出按钮后自动直接下载excel文件。