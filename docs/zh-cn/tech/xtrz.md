##  系统日志

> 在我们实际开发的项目中，对于使用系统时们需要对系统的操作日志进行一些保留，当我们自定义写保留日志方法时，每次都得调用方法，还得去收集一些大量的数据，会造成大量的代码重复，所以引入注解来完成对系统的操作日志的保存。

在使用系统时，在`controller`方法上只需要添加`@Log`注解，就可以用来保存操作日志，使用方法如下：

```java
@Log(value = "添加修改", businessType = BusinessType.INSERTORUPDATE)
@PostMapping("/saveOrUpdate")
public ResultJson saveOrUpdate(......){

   return ResultJson.ok();	
}
```

## `@Log`注解参数说明:

采用enum枚举类来存储对应的操作功能信息

| 参数         | 类型         | 默认值 | 描述                                                         |
| ------------ | ------------ | ------ | ------------------------------------------------------------ |
| value        | String       | 空     | 此纪录的操作说明                                             |
| businessType | BusinessType | OTHER  | 操作功能（`OTHER`其他、`INSERT`新增、`UPDATE`修改、`DELETE`删除、`GRANT`授权、`EXPORT`导出、`IMPORT`导入、`FORCE`强退、`GENCODE`生成代码、`CLEAN`清空数据、`LIST`浏览、`INSERTORUPDATE`添加或修改） |
| operatorType | OperatorType | MANAGE | 操作人类别（`OTHER`其他、`MANAGE`后台用户、`MOBILE`手机端用户） |

<img src="https://1461721871-1307762253.cos.ap-nanjing.myqcloud.com/img/image-20220814224304890.png" alt="image-20220814224304890" style="zoom:33%;" />

<img src="https://1461721871-1307762253.cos.ap-nanjing.myqcloud.com/img/image-20220814224321870.png" alt="image-20220814224321870" style="zoom:33%;" />

## 自定义操作功能

在BusinessType枚举类下自定义声操作功能：（例：自定义增加TEST操作功能）

```java
package tech.niua.core.enums;

/**
 * 业务操作类型
 * 
 */
public enum BusinessType
{
    /**
     * 其它
     */
    OTHER,

    /**
     * 新增
     */
    INSERT,

    /**
     * 修改
     */
    UPDATE,

    /**
     * 删除
     */
    DELETE,

    /**
     * 授权
     */
    GRANT,

    /**
     * 导出
     */
    EXPORT,

    /**
     * 导入
     */
    IMPORT,

    /**
     * 强退
     */
    FORCE,

    /**
     * 生成代码
     */
    GENCODE,
    
    /**
     * 清空数据
     */
    CLEAN,
    /**
     * 浏览
     */
    LIST,
    /**
     * 添加或修改
     */
    INSERTORUPDATE,
    /**
     * 测试
     */
    TEST
}

```

在`controller`方法使用：

```java
@Log(value = "测试", businessType = BusinessType.TEST)
@GetMapping("/test")
public ResultJson test() {
    
   return ResultJson.ok();	
}
```

测试TEST操作就会正常存入系统中。