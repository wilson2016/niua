# 权限注解

> Spring Security默认是在配置类中使用URL进行拦截，想要开启Spring方法级安全，需要在已经添加了@Configuration注解的类上再添加@EnableGlobalMethodSecurity注解，并且开始权限@EnableGlobalMethodSecurity(prePostEnabled = true)，此系统中已经默认开启权限验证，我们只需根据自己的需求配置注解即可。

<img src="https://1461721871-1307762253.cos.ap-nanjing.myqcloud.com/img/image-20220814214533948.png" alt="image-20220814214533948" style="zoom: 33%;" />

- @PreAuthorize --方法之前验证授权
- @PostAuthorize --授权方法之后被执行
- @PostFilter --在方法执行之后执行，用于过滤集合；
- @PreFilter --在方法执行之前执行，用于过滤集合；

此系统中常用注解：

@PreAuthorize 使用该注解的方法，在方法调用时，当前角色必须具有访问该方法的权限

## @PreAuthorize

注解使用方式

1.调用该方法前：该角色必须有systemNotice权限，才能够访问该方法

```java
@PreAuthorize("hasAuthority('/systemNotice')")
@GetMapping("/findById/{id}")
public ResultJson findSystemNoticeById(@PathVariable Long id) {

    return ResultJson.ok();
}
```

2调用方法前：必须有该角色才可以访问该方法

```java
@GetMapping("api/admin")
@PreAuthorize("hasRole('ADMIN')")
public String authAdmin() {
    return "需要ADMIN权限";
}
```

3.调用该方法前：必须是匿名用户才能访问

```java
@GetMapping("api/test")
@PreAuthorize("isAnonymous()")
public String authUser() {
    return "匿名用户访问";
}
```

4.也可以用EL表达式判断,调用该方法前：必须请请求体里面的username=='admin'才可以访问

```java
@GetMapping("api/has")
@PreAuthorize("#sysUser.username == 'admin' ")
public String hasPerm(SysUser sysUser) {
    return "EL测试";
}
```

常见内置表达式：

| 表达                        | 描述                                                         |
| --------------------------- | ------------------------------------------------------------ |
| `hasRole([role])`           | 如果当前主体具有指定角色，则返回`true`。默认情况下，如果提供的角色不以“ROLE_”开头，则会添加该角色。 |
| `hasAnyRole([role1,role2])` | 如果当前主体具有任何提供的角色（以逗号分隔的字符串列表给出），则返回`true`。默认情况下，如果提供的角色不以“ROLE_”开头，则会添加该角色。 |
| `hasAuthority([authority])` | 如果当前主体具有指定的权限，则返回`true`。                   |
| `isAnonymous()`             | 如果当前主体是匿名用户，则返回`true`                         |

