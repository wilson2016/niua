# 框架介绍

---

## 项目简介

NIUA-EASY原名牛啊，由于不可抗力因素，根据牛啊谐音为 ***牛蛙***

NIUA-EASY 是一款基于 `SpringBoot + Vue` 的前后端分离的后台开发管理框架

- 牛蛙官网: [http://www.niua.tech](http://www.niua.tech)
- 代码下载：[Wilson/niua](https://gitee.com/wilson2016/niua)

> NIUA 是一个 Java EE 企业级快速开发管理平台，(基于 `SpringBoot` 、 `String Security` 、 `Jwt` 、 `Mybatis-Plus` 、 `Vue` )
>
> 内置模块包括：
>> 1. 首页模块
>> 2. 第三方服务
      >> 	1. Swagger
>> 	2. Druid
>> 3. 图表展示
>> 4. 组件展示
>> 5. 权限配置
      >> 	1. 权限管理（原资源管理）
>> 	2. 角色管理
>> 	3. 用户管理
>> 	4. 部门管理
>> 	5. 操作日志
>> 6. 定时任务管理
>> 7. 代码生成模块

## 主要特性

- 响应式布局（支持电脑、平板、手机等所有主流设备）
- 一键生成代码功能（包括 `Controller` 、 `Mapper` 、 `Service` 、 `ServiceImpl` 、 `Domain` 、 `Mybatis xml` 、 `Vue` ）
- 支持多数据源，简单配置即可实现切换
- 支持自定义权限
- 支持侧边栏外链跳转
- Maven 多项目依赖，模块及插件分项目，尽量松耦合，方便模块升级、增减模块。
- 国际化支持，服务端及客户端支持
- 日志支持