# 删除

删除功能点击弹出确认框，确认后删除当前行的信息。

函数主体：

```javascript
    deleteHandleClick: function (row) {
      this.$confirm('此操作将永久删除该信息, 是否继续?', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      }).then(() => {
        this.$data.ids.push(row.id);
        this.delete();
      }).catch(() => {
        this.$message({
          type: 'info',
          message: '已取消删除'
        });
      });
    },
    //删除ids中对应的
    delete: function () {
      let data = this.$data.ids
      //获得url尾部,要删除的id部分
      let urlChild = '';
      data.forEach((e) => {
        urlChild += 'ids=' + e + '&'
      });
      urlChild = urlChild.substring(0, urlChild.lastIndexOf('&'));

      let url = this.$data.urls.delete + '/?' + urlChild
      this.axios({
        method: 'GET',
        url: url,
        data: {}
      }).then(res => {
        let code = res.data.code;
        this.$data.ids = [];//清空要删除的id数组
        if (code == 200) {
          this.pageList();
        }
      }).catch(error => {
        console.log(error);
      });
    },
```

后端代码：

```
    /**
    * 删除
    * @param ids
    * @return
    */
    @Log(value = "删除", businessType = BusinessType.DELETE)
    @PreAuthorize("hasAuthority('/test/delete')")
    @ApiImplicitParams({@ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    @GetMapping("/delete")
    public ResultJson delete(@RequestParam("ids") Long[] ids){
        UpdateWrapper<Test> updateWrapper = new UpdateWrapper<>();
        updateWrapper.in("id",ids).set("delete_flag",1);
        boolean update = testService.update(null, updateWrapper);
        if(update) return ResultJson.ok();
        return ResultJson.failure(ResultCode.SERVER_ERROR);
    }

```

参数说明：

- `row`object对象格式，内有操作行的全部信息

总结：确认框点击确定后，会将`row`的id放进ids数组中，在处理删除的函数中，会将ids数组中的数据逐个读出并逐一拼接`'ids='` 和`'&'`将数据绑定到url中，拼接我完成后裁减掉最后一个`'&'`，之后向后端发起请求，后端通过数组接收获取的数据，在数组中遍历id删除对应数据。

调用：

```vue
//与编辑相同，deleteHandleClick获取行数据并绑定到删除按钮的点击事件
	  <el-table-column fixed="right" label="操作">
        <template slot-scope="scope">
          <el-button v-auth="['/test/delete']" size="small"
                     type="text" @click="deleteHandleClick(scope.row)">删除
          </el-button>
        </template>
      </el-table-column>
```

