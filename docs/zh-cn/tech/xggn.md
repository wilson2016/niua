# 修改功能

在修改功能中，会调用两个后端接口，一个用于回显数据，一个用于修改数据。

函数主体：

```javascript
//修改框弹出并回显
editorHandleClick: function (row) {
      this.$data.loading = true;
      this.isEditor = true;
      let url = this.$data.urls.find + "/" + row.id;
      this.axios({
        method: "GET",
        url: url,
        data: {},
      }).then((res) => {
        this.$data.loading = false;
        let code = res.data.code;
        if (code == 200) {
          this.dialogAddFormVisible = true;
          this.resetDialogFormData('dialogForm');
          this.$nextTick(() => {
            this.$data.dialogForm = res.data.data;
          })
        }
      }).catch((error) => {
        console.log(error)
      });
    },
    //与添加调用的函数为同一个
    saveOrUpdate: function () {
      this.$refs['dialogForm'].validate((valid) => {
        if (valid && this.checkDialogForm()) {
          this.axios({
            method: 'POST',
            url: this.$data.urls.saveOrUpdate,
            data: this.dialogForm
          }).then(res => {
            let code = res.data.code
            if (code == 200) {
              this.pageList();
              this.$data.dialogAddFormVisible = false
            } else if (code == 20004) {
              this.$message.error('请先修改数据在更新');
            }
          }).catch(error => {
            console.log(error);
          });
        } else {
          console.log('error submit!!');
          return false;
        }
      });
    },
```

后端代码：

```java
    /**
    *根据id查找
    * @param: id
    * @return
    */
    @PreAuthorize("hasAuthority('/test')")
    @ApiImplicitParams({@ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    @GetMapping("/findById/{id}")
    public ResultJson findTestById(@PathVariable Long id) {
        Test test = testService.getById(id);
        if(test != null){
            return ResultJson.ok(test);
        }
        return ResultJson.failure(ResultCode.BAD_REQUEST);
    }
    
    /**
    * 添加修改
    * @param test
    * @return
    */
    @Log(value = "添加修改", businessType = BusinessType.INSERTORUPDATE)
    @PreAuthorize("hasAuthority('/test/saveOrUpdate')")
    @NoRepeatSubmit
    @ApiImplicitParams({@ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    @PostMapping("/saveOrUpdate")
    public ResultJson saveOrUpdate(@RequestBody Test test){
        boolean flag = testService.saveOrUpdate(test);
        if(flag){
            return ResultJson.ok();
        }
        return ResultJson.failure(ResultCode.NOT_UPDATE);
    }
```

参数说明：

- `row`object对象格式，内有操作行的全部信息
-  `dialogForm`需要传给后台的json数据，里面定义封装好的对象的属性，回显会向其中添加数据

总结：`editorHandleClick`中，将`isEditor`设置为true标识弹出框为修改框，调用通过id查找接口，将`row`的id，即当前操作行的数据的id作为传参，后台通过id查出对应的信息后将实体以json的方式传给前端，在前端将接收到的数据赋给`dialogForm`，完成回显，修改`dialogForm`中的数据后，再通过`saveOrUpdate`修改数据；后端接口添加修改使用的MybatisPlus的saveOrUpdate方法，该方法可以通过判断实体id（主键）是否存在自动完成添加或修改的操作。

调用：

```vue
//通过slot-scope="scope"获取当前操作行对象，绑定点击事件调用editorHandleClick
<el-table-column fixed="right" label="操作">
        <template slot-scope="scope">
          <el-button v-auth="['/test/saveOrUpdate']" size="small"
                     type="text" @click="editorHandleClick(scope.row)">编辑
          </el-button>

          </el-button>
        </template>
      </el-table-column>
//通过绑定确定按钮触发saveOrUpdate
        <el-button type="primary" @click="saveOrUpdate">确 定</el-button>
```
