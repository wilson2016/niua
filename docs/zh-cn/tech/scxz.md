# 上传下载

## 上传实现流程

1. 在需要上传功能的 `vue` 界面引入封装的组件

```vue
import Upload from "@/components/Upload/Upload";
```

或引入多文件上传组件

```vue
import Upload from "@/components/Upload/MultiUpload";
```

2. 使用上传组件

```vue
<Upload :value="form.avatar" @input="handleUpload"></Upload>
```

单文件上传组件可用属性及事件：

> `value` 为双向绑定的值
> 
> `@input` 事件为上传成功后执行的事件
> 
> `fileType` 为允许上传的文件格式，默认为 `image/*`

多文件上传组件可用属性及事件：

> `fileType` 为允许上传的文件格式，默认为 `image/*`
> 
> `limit` 文件上传个数限制
> 
> `value` 文件列表双向绑定值
>
> `@input` 事件为上传成功后执行的事件

3. 组件中的 token 引用

```vue
import { getToken } from '@/utils/auth';
```

> TIPS: 使用封装好的组件不需要引入token，token已经封装在组件中了

4. 上传头像示例

```vue
<Upload :value="form.avatar" @input="handleUpload"></Upload>
```

```javascript
handleUpload: function(val) {
    this.form.avatar = val;
}
```

## 下载实现流程

1. 添加按钮及事件绑定

```vue
<el-button type="primary" @click="handleDownload(scope.row)">下载</el-button>
```

2. 下载事件函数

```js
handleDownload(row) {
    var filename = row.filename;
    var url = row.filePath;
    var suffix = url.substring(url.lastIndexOf("."), url.length);
    const a = document.createElement('a')
    a.setAttribute('download', name + suffix)
    a.setAttribute('target', '_blank')
    a.setAttribute('href', url)
    a.click()
}
```
