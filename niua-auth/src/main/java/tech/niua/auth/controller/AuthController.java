package tech.niua.auth.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import tech.niua.auth.domain.*;
import tech.niua.auth.service.AuthService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import tech.niua.auth.service.IRoleService;
import tech.niua.auth.service.ISysResourceService;
import tech.niua.auth.service.ISysUserService;
import tech.niua.auth.utils.AESEncrypt;
import tech.niua.auth.utils.JwtUtils;
import tech.niua.common.exception.CustomException;
import tech.niua.common.model.ResultCode;
import tech.niua.common.model.ResultJson;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Wangzhen
 * createAt: 2020/5/29
 */
@RestController
@Api(value = "用户权限接口", tags = {"用户权限接口"})
@RequestMapping("/auth")
public class AuthController {
    @Value("${jwt.header}")
    private String tokenHeader;

    @Value("${jwt.tokenHead}")
    private String tokenHead;

    private final AuthService authService;

    @Autowired
    private ISysUserService sysUserService;

    @Autowired
    private IRoleService roleService;

    @Resource
    private JwtUtils jwtUtils;


    @Autowired
    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping(value = "/login")
    @ApiOperation(value = "登录", notes = "登录成功返回token,登录之前请先注册账号")
    public ResultJson<ResponseUserToken> login(
            @Valid @RequestBody SysUser user) throws CustomException {
        user.setUsername(AESEncrypt.aesDecrypt(user.getUsername()));
        user.setPassword(AESEncrypt.aesDecrypt(user.getPassword()));
        final ResponseUserToken response = authService.login(user.getUsername(), user.getPassword());
        response.getUserDetail().setUsername(AESEncrypt.aesEncrypt(user.getUsername()));
        response.setUserDetail(response.getUserDetail());
        return ResultJson.ok(response);
    }

    @GetMapping(value = "/logout")
    @ApiOperation(value = "登出", notes = "退出登录")
    @ApiImplicitParams({@ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    public ResultJson logout(HttpServletRequest request){
        String token = request.getHeader(tokenHeader);
        if (token == null) {
            return ResultJson.failure(ResultCode.UNAUTHORIZED);
        }
        authService.logout(token);
        return ResultJson.ok();
    }

    @GetMapping(value = "/user")
    @ApiOperation(value = "根据token获取用户信息", notes = "根据token获取用户信息")
    @ApiImplicitParams({@ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    public ResultJson getUser(HttpServletRequest request){
        String token = request.getHeader(tokenHeader);
        if (token == null) {
            return ResultJson.failure(ResultCode.UNAUTHORIZED);
        }
        UserInfo userInfo = authService.getUserByToken(token);
        return ResultJson.ok(userInfo);
    }

    //获取用户信息，menu调整
    @GetMapping(value = "/userInfo")
    @ApiOperation(value = "根据token获取用户信息", notes = "根据token获取用户信息")
    @ApiImplicitParams({@ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    public ResultJson userInfo(HttpServletRequest request){
        String token = request.getHeader(tokenHeader);
        if (token == null) {
            return ResultJson.failure(ResultCode.UNAUTHORIZED);
        }
        token = token.substring(tokenHead.length());
        long userId = jwtUtils.getUserIdFromToken(token);
        SysUser user = sysUserService.getById(userId);
        List<SysRole> sysRoles = roleService.selectRolesByUserId(userId);
        //返回用户信息
        UserInfo userInfo = UserInfo.builder().build();
        userInfo.setUsername(AESEncrypt.aesEncrypt(user.getUsername()));
        userInfo.setTel(user.getTel());
        List<String> arrayList = new ArrayList<>();
        for (SysRole sysRole : sysRoles) {
            arrayList.add(sysRole.getName());
        }
        userInfo.setRoles(arrayList);
        return ResultJson.ok(userInfo);
    }

    @PostMapping(value = "/sign")
    @ApiOperation(value = "用户注册")
    public ResultJson sign(@RequestBody SysUser user) {
        if (StringUtils.isAnyBlank(user.getUsername(), user.getPassword())) {
            return ResultJson.failure(ResultCode.BAD_REQUEST);
        }

        return ResultJson.ok(sysUserService.register(user));
    }
//    @GetMapping(value = "refresh")
//    @ApiOperation(value = "刷新token")
//    public ResultJson refreshAndGetAuthenticationToken(
//            HttpServletRequest request){
//        String token = request.getHeader(tokenHeader);
//        ResponseUserToken response = authService.refresh(token);
//        if(response == null) {
//            return ResultJson.failure(ResultCode.BAD_REQUEST, "token无效");
//        } else {
//            return ResultJson.ok(response);
//        }
//    }
}
