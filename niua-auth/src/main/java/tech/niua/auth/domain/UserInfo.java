package tech.niua.auth.domain;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


/**
 * @author : 刘卓
 * createAt: 2022/4/18
 */
@Builder
@Data
public class UserInfo implements Serializable {

    private long id;
    //用户名
    private String username;
    //手机号
    private String tel;
    //是否可用
    private Integer available;
    //权限名
    private List<String> roles;
    //资源信息
    private List<SysResource> resources;

}
