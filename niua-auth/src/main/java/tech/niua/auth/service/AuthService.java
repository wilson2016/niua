package tech.niua.auth.service;

import tech.niua.auth.domain.ResponseUserToken;
import tech.niua.auth.domain.UserDetail;
import tech.niua.auth.domain.UserInfo;
import tech.niua.common.exception.CustomException;

/**
 * @author: Wangzhen
 * createAt: 2020/5/29
 */
public interface AuthService {
    /**
     * 注册用户
     * @param userDetail
     * @return
     */
    UserDetail register(UserDetail userDetail);

    /**
     * 登录
     * @param username
     * @param password
     * @return
     */
    ResponseUserToken login(String username, String password) throws CustomException;

    /**
     * 登出
     * @param token
     */
    void logout(String token);

    /**
     * 刷新Token
     * @param oldToken
     * @return
     */
    ResponseUserToken refresh(String oldToken);

    /**
     * 根据Token获取用户信息
     * @param token
     * @return
     */
    UserInfo getUserByToken(String token);
}
