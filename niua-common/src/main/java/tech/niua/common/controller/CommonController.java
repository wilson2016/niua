package tech.niua.common.controller;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import tech.niua.common.config.NiuaConfig;
import tech.niua.common.config.ServerConfig;
import tech.niua.common.model.ResultCode;
import tech.niua.common.model.ResultJson;
import tech.niua.common.model.UEResJson;
import tech.niua.common.utils.file.FileUploadUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author wangzhen
 * @title: UploadController
 * @projectName niua_easy_parent
 * @description: 通用文件上传类
 * @date 2020/12/19 下午10:51
 */
@RestController
@Api(value = "通用接口", tags = {"通用接口"})
@RequestMapping("/admin/common")
public class CommonController {

    @Autowired
    private ServerConfig serverConfig;

    /**
     * 通用上传请求
     */
    @PostMapping("/upload")
    public ResultJson uploadFile(MultipartFile file) throws Exception {
        try {
            // 上传文件路径
            String filePath = NiuaConfig.getUploadPath();
            // 上传并返回新文件名称
            String fileName = FileUploadUtils.upload(filePath, file);
            String url = serverConfig.getUrl() + fileName;
            Map<String, String> prams = new HashMap<String, String>();
            prams.put("fileName", fileName);
            prams.put("url", url);
            return ResultJson.ok(prams);
        } catch (Exception e) {
            return ResultJson.failure(ResultCode.BAD_REQUEST, e.getMessage());
        }
    }

    /**
     * 通用上传请求
     * @return
     */
    @PostMapping("/editorUpload")
    public JSONObject editorUpload(MultipartFile file) throws Exception {
        try {
            // 上传文件路径
            String filePath = NiuaConfig.getUploadPath();
            // 上传并返回新文件名称
            String fileName = FileUploadUtils.upload(filePath, file);
            String url = serverConfig.getUrl() + fileName;
            Map<String, String> prams = new HashMap<String, String>();
            prams.put("fileName", fileName);
            prams.put("url", url);

            JSONObject json = new JSONObject();
            json.put("errno",0);
            JSONObject dataJson = new JSONObject();
            dataJson.put("url",fileName);
            json.put("data",dataJson);

            return json;
        } catch (Exception e) {
            JSONObject json = new JSONObject();
            json.put("errno",1);
            json.put("message","上传失败");
            return json;
        }
    }

    /**
     * UEditor 通用上传请求
     * @param file
     * @return
     * @throws Exception
     */
    @PostMapping("/uploadUE")
    public String uploadFileUE(MultipartFile file) throws Exception {
        try {
            // 上传文件路径
            String filePath = NiuaConfig.getUploadPath();
            // 上传并返回新文件名称
            String fileName = FileUploadUtils.upload(filePath, file);
            String url = serverConfig.getUrl() + fileName;

            ObjectMapper mp = new ObjectMapper();
            UEResJson neo = new UEResJson();
            neo.setState("SUCCESS");
            neo.setUrl(url); // url
            neo.setTitle(file.getOriginalFilename()); // title
            neo.setOriginal(file.getOriginalFilename()); // alt

            String str = mp.writeValueAsString(neo);
            return str;
        } catch (Exception e) {
            return "";
        }
    }

}
