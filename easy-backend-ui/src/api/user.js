import axios from "axios";
import {AUTH_LOGIN_URL, AUTH_LOGOUT_URL, FIND_USER_INFO} from "@/utils/api";

//用户登录
export function login(data) {
  return axios({
    method: "POST",
    url: AUTH_LOGIN_URL,
    data: data,
  });
}
//获取用户信息
export function getInfo() {
  return axios({
    method: "GET",
    url: FIND_USER_INFO,
    data: {}
  });
}
//退出登录
export function logout() {
  return axios({
    method: "GET",
    url: AUTH_LOGOUT_URL,
    data: {},
  });
}