import Cookies from 'js-cookie'


export function getCurrentAuthority(){
	let authorities = JSON.parse(sessionStorage.getItem("authorities"));
	return authorities;
}

export function setCurrentAuthority(authorities){
	sessionStorage.setItem('authorities', JSON.stringify(authorities));
}

export function removeAuthority(){
	sessionStorage.removeItem('authorities');
}

export function check(authority){
	const current = getCurrentAuthority();
	return current && current.some(item => authority.includes(item.authority));
}

export function setCurrentUsername(username){
	sessionStorage.setItem('username', username)
}

export function getCurrentUsername(){
	return sessionStorage.getItem("username");
}

export function removeCurrentUsername(){
	sessionStorage.removeItem("username");
}

export function setCurrenRole(role){
	sessionStorage.setItem('Role', role)
}

export function getCurrentRole(){
	return sessionStorage.getItem("Role");
}

export function removeCurrentRole(){
	sessionStorage.removeItem("Role");
}


export function isLogin(){
	const current = getCurrentAuthority();
	return current;
}

const TokenKey = 'token'

export function getToken() {
	return Cookies.get(TokenKey)
}

export function setToken(token) {
	return Cookies.set(TokenKey, token)
}

export function removeToken() {
	return Cookies.remove(TokenKey)
}