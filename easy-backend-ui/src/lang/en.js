export default {
    login: {
        title: 'NIUA Management System',
        logIn: 'Log in',
        username: 'Please Enter Username',
        password: 'Please Enter Password',
        need: 'Please fill in the username and password',
        trueActPwd: 'Please enter correct username and password',
        verifyCode: 'verification code',
        falseVC: 'Verification code error',
        vcNull: 'Verification code must be filled',
        userDisabled: "User is disabled",
        tip: 'Tips',
        success: 'Login successful'
    },
    navbar: {
        account: 'Account',
        changePassword: 'Change password',
        logOut: 'Log out',
        resetPassword: 'Reset password',
        oldPassword: 'Old password',
        oldPlace: 'Please enter old password',
        newPassword: 'New password',
        newPlace: 'Please enter new password',
        repeatPassword: 'Repeat password',
        repeatPlace: 'Please enter new password again',
        confirm: 'Confirm',
        reset: 'Reset',
        confirmAgain: 'Please confirm new password',
        resetSuccess: 'Reset password successful',
        passwordLen: 'The length of the user password is 6 to 18 characters',
        diffPassword: 'The passwords entered twice do not match!'
    }
}