const getters = {
  sidebar: state => state.app.sidebar,
  size: state => state.app.size,
  device: state => state.app.device,
  visitedViews: state => state.tagsView.visitedViews,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  introduction: state => state.user.introduction,
  roles: state => state.user.roles,
  permission_routes: state => state.permission.routes,
  cachedViews: state => state.tagsView.cachedViews,
  userDetail: state => state.user.userDetail,
  authorities: state => state.user.authorities,
  language: state => state.app.language,
  clearCancel: state => state.cancelToken.clearCancel,
  pushCancel: state => state.cancelToken.pushCancel,
}
export default getters
