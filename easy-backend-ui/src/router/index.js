import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'


/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: "/login",
    hideInMenus: true,
    component: () => import('@/components/Login'),
  },
  {
    path: "*",
    hideInMenus: true,
    component: () => import('@/views/404'),
  },
  {
    path: '/',
    hideInMenus: true,
    redirect: '/index'
  },
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/components/redirect/index')
      }
    ]
  },
  // {
  //   path: "/redirect",
  //   hideInMenus: true,
  //   component: () => import('@/components/kong/Redirect'),
  // },
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// 重置路由
// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
