import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import router from '@/router'
import {getToken} from "@/utils/auth";
import {
	// Loading,
	Message
} from 'element-ui'
import store from "@/store";

axios.defaults.baseURL = "/niua";

Vue.use(VueAxios, axios, router)

// 超时时间
axios.defaults.timeout = 60000
// http请求拦截器
// var loadinginstace

axios.interceptors.request.use(config => {
	// element ui Loading方法
	// loadinginstace = Loading.service({
	// 	fullscreen: true
	// })
	let token = getToken();
	
	if (!token) {
		router.replace({
			path: "login"
		});
	}
	config.headers = {
		'Authorization': 'Bearer ' + token,
		// 'Content-Type': 'application/x-www-form-urlencoded'
	}
	//重复弹窗，只弹一次
	config.cancelToken = new axios.CancelToken((cancel) => {
		//使用vuex 定义pushCancel，请求进来存入
		store.dispatch('cancelToken/pushCancel', {cancelToken:cancel})
	});

	return config
}, error => {
	// loadinginstace.close()
	Message.error({
		message: '加载超时'
	})
	return Promise.reject(error)
})
// http响应拦截器
axios.interceptors.response.use(data => { // 响应成功关闭loading
	// loadinginstace.close()
	let code = data.data.code;
	if(code == 401){
		Message.warning({
			message: '登录权限失效，请重新登录'
		})
		store.dispatch('user/resetToken')
		store.dispatch('cancelToken/clearCancel');
		router.replace({
			path: "login"
		});
		return Promise.reject(data)
	}
    
	if(code == 500){
		Message.error({
			message: '服务端响应错误，请联系管理员'
		})
		store.dispatch('cancelToken/clearCancel');
	}
	if(code == 403){
		Message.warning({
			message: '你无权操作此功能，请联系管理员添加'
		})
		store.dispatch('cancelToken/clearCancel');
	}

	if(code == 20005){
		Message.warning({
			message: '操作太频繁，请稍后'
		})
	}

	return data
}, error => {
	if (axios.isCancel(error)) {
		// 使用isCancel 判断是否是主动取消请求
		return new Promise(() => {});
	}else {
		// loadinginstace.close()
		Message.error({
			message: '加载失败'
		})
		return Promise.reject(error)
	}
})
