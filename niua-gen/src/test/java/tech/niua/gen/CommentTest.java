package tech.niua.gen;

import org.junit.Test;
import tech.niua.common.model.ResultJson;
import tech.niua.gen.model.TableCommentModel;
import tech.niua.gen.util.HandlerCommentUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @author wangzhen
 * @title: CommentTest
 * @projectName nuia-easy
 * @description: TODO
 * @date 2022/4/14 下午8:14
 */

public class CommentTest {

    @Test
    public void handlerComment(){
        String filed = "quarantine_status";
        String comment = "隔离状态#0:未隔离 #1:转运中 #2:已隔离";
        TableCommentModel tableCommentModel = HandlerCommentUtil.handlerComment(filed, comment);
        System.out.println(ResultJson.ok(tableCommentModel));
    }
}


