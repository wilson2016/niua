package ${cfg.voPackage};

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import tech.niua.common.annotation.Excel;

import java.time.LocalDateTime;
<#assign flag=0 />
<#list table.fields as field >
    <#if field.propertyType == "LocalDate">
import java.time.LocalDate;
        <#break>
    </#if>
    <#if field.customMap.specialFieldFlag>
        <#assign flag=flag+1 />
import ${cfg.pkgName}.${field.customMap.specialFieldCommentModel.beanObjName?uncap_first}.domain.${field.customMap.specialFieldCommentModel.beanObjName};
        <#if flag!=1>
            <#continue />
        </#if>
import java.util.List;
    </#if>
</#list>

/**
* @className: ${table.entityName}
* @description: ${table.comment}
* @author: niua
* @since ${.now?string("yyyy-MM-dd HH:mm:ss")}
*
*/
@Builder
@Data
public class ${table.entityName}VO {
<#list table.fields as field >

    <#if field.keyFlag>
    private ${field.propertyType} ${field.propertyName};
    </#if>
    <#if field.propertyName == "updateTime">
    private ${field.propertyType} ${field.propertyName};
    </#if>
    <#if field.propertyName == "createTime">
    private ${field.propertyType} ${field.propertyName};

    private LocalDateTime ${field.propertyName}Begin;
    private LocalDateTime ${field.propertyName}End;

    </#if>
    <#if field.propertyName != "createTime" && field.propertyName != "updateTime" && !field.keyFlag>
    private ${field.propertyType} ${field.propertyName};
    </#if>
    <#if field.customMap.relationFlag>
    private String ${field.customMap.relationCommentModel.aliasVar};
    </#if>
    <#if field.customMap.specialFieldFlag>
    private List<${field.customMap.specialFieldCommentModel.beanObjName}> ${field.customMap.specialFieldCommentModel.beanObjName ?uncap_first}List;
    </#if>
</#list>

}
