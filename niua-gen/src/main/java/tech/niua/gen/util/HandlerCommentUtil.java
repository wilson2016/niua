package tech.niua.gen.util;

import tech.niua.common.model.ResultJson;
import tech.niua.gen.model.RelationCommentModel;
import tech.niua.gen.model.SpecialFieldCommentModel;
import tech.niua.gen.model.TableCommentModel;

import java.util.HashMap;
import java.util.Locale;

/**
 * @author wangzhen
 * @title: HandlerCommentUtil
 * @projectName nuia-easy
 * @description: 数据库注释处理工具
 * @date 2022/4/15 上午8:59
 */
public class HandlerCommentUtil {
    /**
     * 数据库字段注释处理
     * @param filed
     * @param comment
     * @return
     * lihongsen
     * 优化中文：和去除空格
     * @modifyDate 2022/4/23
     */
    public static TableCommentModel handlerComment(String filed, String comment){
        TableCommentModel tableComment = null;

        if(filed.contains("_status")){
            //字段注释去空格
            comment = comment.replaceAll(" ","");
            //字段注释修改不合格字符
            comment = comment.replaceAll("：",":");
            //字符串类型的状态键值对
            String[] strAttr = comment.split("\\#");
            //去掉前后空格
            comment.trim();
            //存储分割后状态
            HashMap<String, String> kvMap = new HashMap<>();
            tableComment = new TableCommentModel();
            for (int i = 0; i < strAttr.length; i++) {
                if (i == 0) {
                    tableComment.setLabel(strAttr[i]);
                }else {
                    String[] kvAttr = strAttr[i].split("\\:");
                    kvMap.put(kvAttr[0], kvAttr[1]);
                }
            }
            tableComment.setParams(kvMap);
        }
        return tableComment;

    }

    public static RelationCommentModel handlerRelationComment(String field, String comment){
        RelationCommentModel relationComment = null;
        if(comment.contains("#join#")){
            //字段注释去空格
            comment = comment.replaceAll(" ","");
            //字段注释修改不合格字符
            comment = comment.replaceAll("：",":");
            //字符串类型的状态键值对
            String[] strAttr = comment.split("\\#");
            //去掉前后空格
            comment.trim();
            //存储分割后状态
//            HashMap<String, String> kvMap = new HashMap<>();
            relationComment = new RelationCommentModel();
            relationComment.setFieldInfo(strAttr[0]);
            relationComment.setTableName(strAttr[2]);
            relationComment.setTablePrimary(strAttr[3]);

            String showFieldRes = "";
            if(strAttr[4].contains("_")){
                String[] showField = strAttr[4].split("_");
                for(int i = 0; i < showField.length; i++){
                    if(i == 0){
                        showFieldRes += showField[i];
                        continue;
                    }
                    showFieldRes += showField[i].substring(0, 1).toUpperCase();
                    showFieldRes += showField[i].substring(1);
                }
            }else{
                showFieldRes = strAttr[4];
            }

            relationComment.setShowFieldBean(showFieldRes);
            relationComment.setShowFieldxml(strAttr[4]);
            relationComment.setShowInfo(strAttr[5]);

            String[] strDash = strAttr[2].split("_");
            String str = "";
            for(int i = 0; i < strDash.length; i++){
                if(i==0){
                    continue;
                }
                strDash[i] = strDash[i].substring(0, 1).toUpperCase() + strDash[i].substring(1);
                str += strDash[i];
            }
            relationComment.setBeanObjName(str);

            String aliasStr = strAttr[2].substring(strAttr[2].indexOf("_", 0) + 1);
            relationComment.setShowAlias(aliasStr + "_" + strAttr[4]);

            String varStr = str.substring(0,1).toLowerCase() + str.substring(1);
            if(strAttr[4].contains("_")){
                String[] tmp = strAttr[4].split("_");
                for(int i = 0; i < tmp.length; i++){
                    tmp[i] = tmp[i].substring(0, 1).toUpperCase() + tmp[i].substring(1);
                    varStr += tmp[i];
                }
            } else{
                varStr += strAttr[4].substring(0, 1).toUpperCase() + strAttr[4].substring(1);
            }
            relationComment.setAliasVar(varStr);
        }
        return relationComment;
    }

    public static SpecialFieldCommentModel handlerSpecialComment(String field, String comment) throws Exception {
        SpecialFieldCommentModel specialFieldCommentModel = null;
        if(field.equals("relation_mark")){
            comment = comment.replaceAll(" ", "");
            comment = comment.replaceAll("！", "!");
            String[] strAttr = comment.split("\\#");
            comment.trim();
            if(!strAttr[0].equals("!") || !strAttr[1].equals("rel") || strAttr[2].equals("")){
                throw new Exception("relation_mark字段注释不合法!");
            }
            specialFieldCommentModel = new SpecialFieldCommentModel();
            specialFieldCommentModel.setFieldInfo(strAttr[0]);
            specialFieldCommentModel.setRelationTableName(strAttr[2]);
            String[] strDash = strAttr[2].split("_");
            String str = "";
            for(int i = 0; i < strDash.length; i++){
                if(i==0){
                    continue;
                }
                strDash[i] = strDash[i].substring(0, 1).toUpperCase() + strDash[i].substring(1);
                str += strDash[i];
            }
            specialFieldCommentModel.setBeanObjName(str);
        }
        return specialFieldCommentModel;
    }
}
