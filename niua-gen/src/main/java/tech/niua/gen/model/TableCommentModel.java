package tech.niua.gen.model;

import lombok.Data;

import java.util.Map;

/**
 * @author wangzhen
 * @title: TableCommentModel
 * @projectName nuia-easy
 * @description: 映射数据库字段注释转换的参数
 * @date 2022/4/15 上午8:56
 */
@Data
public class TableCommentModel {

    private String label; //字段名称
    private Map<String, String> params;


    @Override
    public String toString() {
        return "StatusGen{" +
                "label='" + label + '\'' +
                ", params=" + params +
                '}';
    }
}
