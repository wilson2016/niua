package tech.niua.gen.model;

import lombok.Data;

@Data
public class SpecialFieldCommentModel {

    // 数据库字段 备注 (should be !)
    private String fieldInfo;

    // 表名称
    private String relationTableName;

    private String beanObjName;
}
