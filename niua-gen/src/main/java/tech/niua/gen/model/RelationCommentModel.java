package tech.niua.gen.model;

import lombok.Data;

@Data
public class RelationCommentModel {
    // 数据库字段 备注
    private String fieldInfo;

    // 副表名称
    private String tableName;

    // 副表要关联的字段
    private String tablePrimary;

    // 副表要展示的字段
    private String showFieldBean;
    private String showFieldxml;

    // 副表要展示字段的 信息
    private String showInfo;

    // xml文件中 别名样子 example: classes_name
    private String showAlias;

    // 与别名对应的JavaBean格式的变量名称 example: classesName
    private String aliasVar;

    private String beanObjName;
}
